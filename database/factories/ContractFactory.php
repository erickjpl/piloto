<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Config\Heritages\Contract;
use Faker\Generator as Faker;

$factory->define(Contract::class, function (Faker $faker) {

    return [
        'contract_date' => $faker->date('Y-m-d H:i:s'),
        'salary' => $faker->randomDigitNotNull,
        'employee_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Products\Sales\Inventory;
use Faker\Generator as Faker;

$factory->define(Inventory::class, function (Faker $faker) {

    return [
        'arrival' => $faker->date('Y-m-d H:i:s'),
        'due_date' => $faker->date('Y-m-d H:i:s'),
        'product_presentation' => $faker->word,
        'wholesale_quantity' => $faker->word,
        'purchase_price' => $faker->randomDigitNotNull,
        'status' => $faker->word,
        'observation' => $faker->text,
        'product_id' => $faker->word,
        'heritage_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

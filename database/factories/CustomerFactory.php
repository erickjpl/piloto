<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Customers\Customer;
use Faker\Generator as Faker;

$factory->define(Customer::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'lastname' => $faker->word,
        'dni' => $faker->randomDigitNotNull,
        'phone' => $faker->randomDigitNotNull,
        'email' => $faker->word,
        'address' => $faker->word,
        'user_id' => $faker->word,
        'customer_type_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Config\Suscribes\Plan;
use Faker\Generator as Faker;

$factory->define(Plan::class, function (Faker $faker) {

    return [
        'plan' => $faker->word,
        'slug' => $faker->word,
        'description' => $faker->word,
        'time_frame' => $faker->word,
        'price' => $faker->randomDigitNotNull,
        'product_presentation' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

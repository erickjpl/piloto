<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Products\Aditionals\Promotion;
use Faker\Generator as Faker;

$factory->define(Promotion::class, function (Faker $faker) {

    return [
        'promotion' => $faker->word,
        'slug' => $faker->word,
        'description' => $faker->word,
        'status' => $faker->word,
        'promotion_type_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

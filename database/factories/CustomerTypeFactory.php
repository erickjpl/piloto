<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Customers\CustomerType;
use Faker\Generator as Faker;

$factory->define(CustomerType::class, function (Faker $faker) {

    return [
        'type' => $faker->word,
        'slug' => $faker->word,
        'description' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Billings\BillingDetail;
use Faker\Generator as Faker;

$factory->define(BillingDetail::class, function (Faker $faker) {

    return [
        'quantity' => $faker->randomDigitNotNull,
        'tax' => $faker->randomDigitNotNull,
        'price' => $faker->randomDigitNotNull,
        'promotion_id' => $faker->word,
        'billing_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

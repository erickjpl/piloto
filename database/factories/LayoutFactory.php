<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Config\Heritages\Layout;
use Faker\Generator as Faker;

$factory->define(Layout::class, function (Faker $faker) {

    return [
        'layout' => $faker->word,
        'slug' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Products\Sales\Sale;
use Faker\Generator as Faker;

$factory->define(Sale::class, function (Faker $faker) {

    return [
        'type_sale' => $faker->word,
        'condition' => $faker->word,
        'price' => $faker->randomDigitNotNull,
        'inventory_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Config\Locale\City;
use Faker\Generator as Faker;

$factory->define(City::class, function (Faker $faker) {

    return [
        'city' => $faker->word,
        'slug' => $faker->word,
        'capital' => $faker->word,
        'state_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

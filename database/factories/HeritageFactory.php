<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Config\Heritages\Heritage;
use Faker\Generator as Faker;

$factory->define(Heritage::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'slug' => $faker->word,
        'phone' => $faker->word,
        'address' => $faker->word,
        'email' => $faker->word,
        'zip_code' => $faker->word,
        'headquarter' => $faker->randomDigitNotNull,
        'user_id' => $faker->word,
        'layout_id' => $faker->word,
        'city_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

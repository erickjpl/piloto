<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Products\Aditionals\Service;
use Faker\Generator as Faker;

$factory->define(Service::class, function (Faker $faker) {

    return [
        'service' => $faker->word,
        'slug' => $faker->word,
        'description' => $faker->word,
        'type_service' => $faker->word,
        'price' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

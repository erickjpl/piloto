<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Config\Suscribes\Subscribe;
use Faker\Generator as Faker;

$factory->define(Subscribe::class, function (Faker $faker) {

    return [
        'start_date' => $faker->date('Y-m-d H:i:s'),
        'end_date' => $faker->date('Y-m-d H:i:s'),
        'status' => $faker->word,
        'price' => $faker->randomDigitNotNull,
        'user_id' => $faker->word,
        'plan_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

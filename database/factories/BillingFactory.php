<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Billings\Billing;
use Faker\Generator as Faker;

$factory->define(Billing::class, function (Faker $faker) {

    return [
        'way_paying' => $faker->word,
        'employee_id' => $faker->word,
        'customer_id' => $faker->word,
        'promotion_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

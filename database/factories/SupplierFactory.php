<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Products\Config\Supplier;
use Faker\Generator as Faker;

$factory->define(Supplier::class, function (Faker $faker) {

    return [
        'supplier' => $faker->word,
        'slug' => $faker->word,
        'email' => $faker->word,
        'address' => $faker->word,
        'phone' => $faker->word,
        'zip_code' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Profile\Employee;
use Faker\Generator as Faker;

$factory->define(Employee::class, function (Faker $faker) {

    return [
        'first_name' => $faker->word,
        'last_name' => $faker->word,
        'dni' => $faker->randomDigitNotNull,
        'phone' => $faker->word,
        'birth' => $faker->word,
        'status' => $faker->word,
        'user_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

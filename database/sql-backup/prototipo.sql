-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-05-2010 a las 06:35:20
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prototipo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `billings`
--

CREATE TABLE `billings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `way_paying` enum('efectivo','cheque','tarjeta de credito','nota de credito','cupon') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `billing_details`
--

CREATE TABLE `billing_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL,
  `tax` double(10,2) UNSIGNED NOT NULL,
  `price` double(10,2) UNSIGNED NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promotion` double(10,2) DEFAULT NULL COMMENT 'Monto de la promoción',
  `billing_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brands`
--

CREATE TABLE `brands` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `brand` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cities`
--

CREATE TABLE `cities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `capital` bigint(20) UNSIGNED NOT NULL,
  `state_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cities`
--

INSERT INTO `cities` (`id`, `city`, `slug`, `capital`, `state_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Maroa', 'maroa', 0, 1, '2020-05-04 07:25:08', '2020-05-04 07:25:08', NULL),
(2, 'Puerto Ayacucho', 'puerto-ayacucho', 1, 1, '2020-05-04 07:25:08', '2020-05-04 07:25:08', NULL),
(3, 'San Fernando de Atabapo', 'san-fernando-de-atabapo', 0, 1, '2020-05-04 07:25:08', '2020-05-04 07:25:08', NULL),
(4, 'Anaco', 'anaco', 0, 2, '2020-05-04 07:25:09', '2020-05-04 07:25:09', NULL),
(5, 'Aragua de Barcelona', 'aragua-de-barcelona', 0, 2, '2020-05-04 07:25:09', '2020-05-04 07:25:09', NULL),
(6, 'Barcelona', 'barcelona', 1, 2, '2020-05-04 07:25:09', '2020-05-04 07:25:09', NULL),
(7, 'Boca de Uchire', 'boca-de-uchire', 0, 2, '2020-05-04 07:25:09', '2020-05-04 07:25:09', NULL),
(8, 'Cantaura', 'cantaura', 0, 2, '2020-05-04 07:25:09', '2020-05-04 07:25:09', NULL),
(9, 'Clarines', 'clarines', 0, 2, '2020-05-04 07:25:09', '2020-05-04 07:25:09', NULL),
(10, 'El Chaparro', 'el-chaparro', 0, 2, '2020-05-04 07:25:09', '2020-05-04 07:25:09', NULL),
(11, 'El Pao Anzoátegui', 'el-pao-anzoategui', 0, 2, '2020-05-04 07:25:09', '2020-05-04 07:25:09', NULL),
(12, 'El Tigre', 'el-tigre', 0, 2, '2020-05-04 07:25:09', '2020-05-04 07:25:09', NULL),
(13, 'El Tigrito', 'el-tigrito', 0, 2, '2020-05-04 07:25:09', '2020-05-04 07:25:09', NULL),
(14, 'Guanape', 'guanape', 0, 2, '2020-05-04 07:25:09', '2020-05-04 07:25:09', NULL),
(15, 'Guanta', 'guanta', 0, 2, '2020-05-04 07:25:09', '2020-05-04 07:25:09', NULL),
(16, 'Lechería', 'lecheria', 0, 2, '2020-05-04 07:25:09', '2020-05-04 07:25:09', NULL),
(17, 'Onoto', 'onoto', 0, 2, '2020-05-04 07:25:09', '2020-05-04 07:25:09', NULL),
(18, 'Pariaguán', 'pariaguan', 0, 2, '2020-05-04 07:25:09', '2020-05-04 07:25:09', NULL),
(19, 'Píritu', 'piritu', 0, 2, '2020-05-04 07:25:09', '2020-05-04 07:25:09', NULL),
(20, 'Puerto La Cruz', 'puerto-la-cruz', 0, 2, '2020-05-04 07:25:09', '2020-05-04 07:25:09', NULL),
(21, 'Puerto Píritu', 'puerto-piritu', 0, 2, '2020-05-04 07:25:09', '2020-05-04 07:25:09', NULL),
(22, 'Sabana de Uchire', 'sabana-de-uchire', 0, 2, '2020-05-04 07:25:09', '2020-05-04 07:25:09', NULL),
(23, 'San Mateo Anzoátegui', 'san-mateo-anzoategui', 0, 2, '2020-05-04 07:25:09', '2020-05-04 07:25:09', NULL),
(24, 'San Pablo Anzoátegui', 'san-pablo-anzoategui', 0, 2, '2020-05-04 07:25:09', '2020-05-04 07:25:09', NULL),
(25, 'San Tomé', 'san-tome', 0, 2, '2020-05-04 07:25:09', '2020-05-04 07:25:09', NULL),
(26, 'Santa Ana de Anzoátegui', 'santa-ana-de-anzoategui', 0, 2, '2020-05-04 07:25:09', '2020-05-04 07:25:09', NULL),
(27, 'Santa Fe Anzoátegui', 'santa-fe-anzoategui', 0, 2, '2020-05-04 07:25:09', '2020-05-04 07:25:09', NULL),
(28, 'Santa Rosa', 'santa-rosa', 0, 2, '2020-05-04 07:25:09', '2020-05-04 07:25:09', NULL),
(29, 'Soledad', 'soledad', 0, 2, '2020-05-04 07:25:09', '2020-05-04 07:25:09', NULL),
(30, 'Urica', 'urica', 0, 2, '2020-05-04 07:25:10', '2020-05-04 07:25:10', NULL),
(31, 'Valle de Guanape', 'valle-de-guanape', 0, 2, '2020-05-04 07:25:10', '2020-05-04 07:25:10', NULL),
(32, 'Achaguas', 'achaguas', 0, 3, '2020-05-04 07:25:10', '2020-05-04 07:25:10', NULL),
(33, 'Biruaca', 'biruaca', 0, 3, '2020-05-04 07:25:10', '2020-05-04 07:25:10', NULL),
(34, 'Bruzual', 'bruzual', 0, 3, '2020-05-04 07:25:10', '2020-05-04 07:25:10', NULL),
(35, 'El Amparo', 'el-amparo', 0, 3, '2020-05-04 07:25:10', '2020-05-04 07:25:10', NULL),
(36, 'El Nula', 'el-nula', 0, 3, '2020-05-04 07:25:10', '2020-05-04 07:25:10', NULL),
(37, 'Elorza', 'elorza', 0, 3, '2020-05-04 07:25:10', '2020-05-04 07:25:10', NULL),
(38, 'Guasdualito', 'guasdualito', 0, 3, '2020-05-04 07:25:10', '2020-05-04 07:25:10', NULL),
(39, 'Mantecal', 'mantecal', 0, 3, '2020-05-04 07:25:10', '2020-05-04 07:25:10', NULL),
(40, 'Puerto Páez', 'puerto-paez', 0, 3, '2020-05-04 07:25:10', '2020-05-04 07:25:10', NULL),
(41, 'San Fernando de Apure', 'san-fernando-de-apure', 1, 3, '2020-05-04 07:25:10', '2020-05-04 07:25:10', NULL),
(42, 'San Juan de Payara', 'san-juan-de-payara', 0, 3, '2020-05-04 07:25:10', '2020-05-04 07:25:10', NULL),
(43, 'Barbacoas', 'barbacoas', 0, 4, '2020-05-04 07:25:10', '2020-05-04 07:25:10', NULL),
(44, 'Cagua', 'cagua', 0, 4, '2020-05-04 07:25:10', '2020-05-04 07:25:10', NULL),
(45, 'Camatagua', 'camatagua', 0, 4, '2020-05-04 07:25:10', '2020-05-04 07:25:10', NULL),
(46, 'Choroní', 'choroni', 0, 4, '2020-05-04 07:25:10', '2020-05-04 07:25:10', NULL),
(47, 'Colonia Tovar', 'colonia-tovar', 0, 4, '2020-05-04 07:25:10', '2020-05-04 07:25:10', NULL),
(48, 'El Consejo', 'el-consejo', 0, 4, '2020-05-04 07:25:10', '2020-05-04 07:25:10', NULL),
(49, 'La Victoria', 'la-victoria', 0, 4, '2020-05-04 07:25:10', '2020-05-04 07:25:10', NULL),
(50, 'Las Tejerías', 'las-tejerias', 0, 4, '2020-05-04 07:25:10', '2020-05-04 07:25:10', NULL),
(51, 'Magdaleno', 'magdaleno', 0, 4, '2020-05-04 07:25:10', '2020-05-04 07:25:10', NULL),
(52, 'Maracay', 'maracay', 1, 4, '2020-05-04 07:25:10', '2020-05-04 07:25:10', NULL),
(53, 'Ocumare de La Costa', 'ocumare-de-la-costa', 0, 4, '2020-05-04 07:25:10', '2020-05-04 07:25:10', NULL),
(54, 'Palo Negro', 'palo-negro', 0, 4, '2020-05-04 07:25:11', '2020-05-04 07:25:11', NULL),
(55, 'San Casimiro', 'san-casimiro', 0, 4, '2020-05-04 07:25:11', '2020-05-04 07:25:11', NULL),
(56, 'San Mateo', 'san-mateo', 0, 4, '2020-05-04 07:25:11', '2020-05-04 07:25:11', NULL),
(57, 'San Sebastián', 'san-sebastian', 0, 4, '2020-05-04 07:25:11', '2020-05-04 07:25:11', NULL),
(58, 'Santa Cruz de Aragua', 'santa-cruz-de-aragua', 0, 4, '2020-05-04 07:25:11', '2020-05-04 07:25:11', NULL),
(59, 'Tocorón', 'tocoron', 0, 4, '2020-05-04 07:25:11', '2020-05-04 07:25:11', NULL),
(60, 'Turmero', 'turmero', 0, 4, '2020-05-04 07:25:11', '2020-05-04 07:25:11', NULL),
(61, 'Villa de Cura', 'villa-de-cura', 0, 4, '2020-05-04 07:25:11', '2020-05-04 07:25:11', NULL),
(62, 'Zuata', 'zuata', 0, 4, '2020-05-04 07:25:11', '2020-05-04 07:25:11', NULL),
(63, 'Barinas', 'barinas', 1, 5, '2020-05-04 07:25:11', '2020-05-04 07:25:11', NULL),
(64, 'Barinitas', 'barinitas', 0, 5, '2020-05-04 07:25:11', '2020-05-04 07:25:11', NULL),
(65, 'Barrancas', 'barrancas', 0, 5, '2020-05-04 07:25:11', '2020-05-04 07:25:11', NULL),
(66, 'Calderas', 'calderas', 0, 5, '2020-05-04 07:25:11', '2020-05-04 07:25:11', NULL),
(67, 'Capitanejo', 'capitanejo', 0, 5, '2020-05-04 07:25:11', '2020-05-04 07:25:11', NULL),
(68, 'Ciudad Bolivia', 'ciudad-bolivia', 0, 5, '2020-05-04 07:25:11', '2020-05-04 07:25:11', NULL),
(69, 'El Cantón', 'el-canton', 0, 5, '2020-05-04 07:25:11', '2020-05-04 07:25:11', NULL),
(70, 'Las Veguitas', 'las-veguitas', 0, 5, '2020-05-04 07:25:11', '2020-05-04 07:25:11', NULL),
(71, 'Libertad de Barinas', 'libertad-de-barinas', 0, 5, '2020-05-04 07:25:11', '2020-05-04 07:25:11', NULL),
(72, 'Sabaneta', 'sabaneta', 0, 5, '2020-05-04 07:25:11', '2020-05-04 07:25:11', NULL),
(73, 'Santa Bárbara de Barinas', 'santa-barbara-de-barinas', 0, 5, '2020-05-04 07:25:11', '2020-05-04 07:25:11', NULL),
(74, 'Socopó', 'socopo', 0, 5, '2020-05-04 07:25:11', '2020-05-04 07:25:11', NULL),
(75, 'Caicara del Orinoco', 'caicara-del-orinoco', 0, 6, '2020-05-04 07:25:11', '2020-05-04 07:25:11', NULL),
(76, 'Canaima', 'canaima', 0, 6, '2020-05-04 07:25:11', '2020-05-04 07:25:11', NULL),
(77, 'Ciudad Bolívar', 'ciudad-bolivar', 1, 6, '2020-05-04 07:25:11', '2020-05-04 07:25:11', NULL),
(78, 'Ciudad Piar', 'ciudad-piar', 0, 6, '2020-05-04 07:25:11', '2020-05-04 07:25:11', NULL),
(79, 'El Callao', 'el-callao', 0, 6, '2020-05-04 07:25:11', '2020-05-04 07:25:11', NULL),
(80, 'El Dorado', 'el-dorado', 0, 6, '2020-05-04 07:25:12', '2020-05-04 07:25:12', NULL),
(81, 'El Manteco', 'el-manteco', 0, 6, '2020-05-04 07:25:12', '2020-05-04 07:25:12', NULL),
(82, 'El Palmar', 'el-palmar', 0, 6, '2020-05-04 07:25:12', '2020-05-04 07:25:12', NULL),
(83, 'El Pao', 'el-pao', 0, 6, '2020-05-04 07:25:12', '2020-05-04 07:25:12', NULL),
(84, 'Guasipati', 'guasipati', 0, 6, '2020-05-04 07:25:12', '2020-05-04 07:25:12', NULL),
(85, 'Guri', 'guri', 0, 6, '2020-05-04 07:25:12', '2020-05-04 07:25:12', NULL),
(86, 'La Paragua', 'la-paragua', 0, 6, '2020-05-04 07:25:12', '2020-05-04 07:25:12', NULL),
(87, 'Matanzas', 'matanzas', 0, 6, '2020-05-04 07:25:12', '2020-05-04 07:25:12', NULL),
(88, 'Puerto Ordaz', 'puerto-ordaz', 0, 6, '2020-05-04 07:25:12', '2020-05-04 07:25:12', NULL),
(89, 'San Félix', 'san-felix', 0, 6, '2020-05-04 07:25:12', '2020-05-04 07:25:12', NULL),
(90, 'Santa Elena de Uairén', 'santa-elena-de-uairen', 0, 6, '2020-05-04 07:25:12', '2020-05-04 07:25:12', NULL),
(91, 'Tumeremo', 'tumeremo', 0, 6, '2020-05-04 07:25:12', '2020-05-04 07:25:12', NULL),
(92, 'Unare', 'unare', 0, 6, '2020-05-04 07:25:12', '2020-05-04 07:25:12', NULL),
(93, 'Upata', 'upata', 0, 6, '2020-05-04 07:25:12', '2020-05-04 07:25:12', NULL),
(94, 'Bejuma', 'bejuma', 0, 7, '2020-05-04 07:25:12', '2020-05-04 07:25:12', NULL),
(95, 'Belén', 'belen', 0, 7, '2020-05-04 07:25:12', '2020-05-04 07:25:12', NULL),
(96, 'Campo de Carabobo', 'campo-de-carabobo', 0, 7, '2020-05-04 07:25:12', '2020-05-04 07:25:12', NULL),
(97, 'Canoabo', 'canoabo', 0, 7, '2020-05-04 07:25:12', '2020-05-04 07:25:12', NULL),
(98, 'Central Tacarigua', 'central-tacarigua', 0, 7, '2020-05-04 07:25:12', '2020-05-04 07:25:12', NULL),
(99, 'Chirgua', 'chirgua', 0, 7, '2020-05-04 07:25:12', '2020-05-04 07:25:12', NULL),
(100, 'Ciudad Alianza', 'ciudad-alianza', 0, 7, '2020-05-04 07:25:12', '2020-05-04 07:25:12', NULL),
(101, 'El Palito', 'el-palito', 0, 7, '2020-05-04 07:25:12', '2020-05-04 07:25:12', NULL),
(102, 'Guacara', 'guacara', 0, 7, '2020-05-04 07:25:12', '2020-05-04 07:25:12', NULL),
(103, 'Guigue', 'guigue', 0, 7, '2020-05-04 07:25:12', '2020-05-04 07:25:12', NULL),
(104, 'Las Trincheras', 'las-trincheras', 0, 7, '2020-05-04 07:25:12', '2020-05-04 07:25:12', NULL),
(105, 'Los Guayos', 'los-guayos', 0, 7, '2020-05-04 07:25:13', '2020-05-04 07:25:13', NULL),
(106, 'Mariara', 'mariara', 0, 7, '2020-05-04 07:25:13', '2020-05-04 07:25:13', NULL),
(107, 'Miranda', 'miranda', 0, 7, '2020-05-04 07:25:13', '2020-05-04 07:25:13', NULL),
(108, 'Montalbán', 'montalban', 0, 7, '2020-05-04 07:25:13', '2020-05-04 07:25:13', NULL),
(109, 'Morón', 'moron', 0, 7, '2020-05-04 07:25:13', '2020-05-04 07:25:13', NULL),
(110, 'Naguanagua', 'naguanagua', 0, 7, '2020-05-04 07:25:13', '2020-05-04 07:25:13', NULL),
(111, 'Puerto Cabello', 'puerto-cabello', 0, 7, '2020-05-04 07:25:13', '2020-05-04 07:25:13', NULL),
(112, 'San Joaquín', 'san-joaquin', 0, 7, '2020-05-04 07:25:13', '2020-05-04 07:25:13', NULL),
(113, 'Tocuyito', 'tocuyito', 0, 7, '2020-05-04 07:25:13', '2020-05-04 07:25:13', NULL),
(114, 'Urama', 'urama', 0, 7, '2020-05-04 07:25:13', '2020-05-04 07:25:13', NULL),
(115, 'Valencia', 'valencia', 1, 7, '2020-05-04 07:25:13', '2020-05-04 07:25:13', NULL),
(116, 'Vigirimita', 'vigirimita', 0, 7, '2020-05-04 07:25:13', '2020-05-04 07:25:13', NULL),
(117, 'Aguirre', 'aguirre', 0, 8, '2020-05-04 07:25:13', '2020-05-04 07:25:13', NULL),
(118, 'Apartaderos Cojedes', 'apartaderos-cojedes', 0, 8, '2020-05-04 07:25:13', '2020-05-04 07:25:13', NULL),
(119, 'Arismendi', 'arismendi', 0, 8, '2020-05-04 07:25:13', '2020-05-04 07:25:13', NULL),
(120, 'Camuriquito', 'camuriquito', 0, 8, '2020-05-04 07:25:13', '2020-05-04 07:25:13', NULL),
(121, 'El Baúl', 'el-baul', 0, 8, '2020-05-04 07:25:13', '2020-05-04 07:25:13', NULL),
(122, 'El Limón', 'el-limon', 0, 8, '2020-05-04 07:25:13', '2020-05-04 07:25:13', NULL),
(123, 'El Pao Cojedes', 'el-pao-cojedes', 0, 8, '2020-05-04 07:25:13', '2020-05-04 07:25:13', NULL),
(124, 'El Socorro', 'el-socorro', 0, 8, '2020-05-04 07:25:13', '2020-05-04 07:25:13', NULL),
(125, 'La Aguadita', 'la-aguadita', 0, 8, '2020-05-04 07:25:13', '2020-05-04 07:25:13', NULL),
(126, 'Las Vegas', 'las-vegas', 0, 8, '2020-05-04 07:25:13', '2020-05-04 07:25:13', NULL),
(127, 'Libertad de Cojedes', 'libertad-de-cojedes', 0, 8, '2020-05-04 07:25:13', '2020-05-04 07:25:13', NULL),
(128, 'Mapuey', 'mapuey', 0, 8, '2020-05-04 07:25:13', '2020-05-04 07:25:13', NULL),
(129, 'Piñedo', 'pinedo', 0, 8, '2020-05-04 07:25:13', '2020-05-04 07:25:13', NULL),
(130, 'Samancito', 'samancito', 0, 8, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(131, 'San Carlos', 'san-carlos', 1, 8, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(132, 'Sucre', 'sucre', 0, 8, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(133, 'Tinaco', 'tinaco', 0, 8, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(134, 'Tinaquillo', 'tinaquillo', 0, 8, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(135, 'Vallecito', 'vallecito', 0, 8, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(136, 'Tucupita', 'tucupita', 1, 9, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(137, 'Caracas', 'caracas', 1, 10, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(138, 'El Junquito', 'el-junquito', 0, 10, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(139, 'Adícora', 'adicora', 0, 11, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(140, 'Boca de Aroa', 'boca-de-aroa', 0, 11, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(141, 'Cabure', 'cabure', 0, 11, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(142, 'Capadare', 'capadare', 0, 11, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(143, 'Capatárida', 'capatarida', 0, 11, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(144, 'Chichiriviche', 'chichiriviche', 0, 11, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(145, 'Churuguara', 'churuguara', 0, 11, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(146, 'Coro', 'coro', 1, 11, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(147, 'Cumarebo', 'cumarebo', 0, 11, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(148, 'Dabajuro', 'dabajuro', 0, 11, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(149, 'Judibana', 'judibana', 0, 11, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(150, 'La Cruz de Taratara', 'la-cruz-de-taratara', 0, 11, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(151, 'La Vela de Coro', 'la-vela-de-coro', 0, 11, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(152, 'Los Taques', 'los-taques', 0, 11, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(153, 'Maparari', 'maparari', 0, 11, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(154, 'Mene de Mauroa', 'mene-de-mauroa', 0, 11, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(155, 'Mirimire', 'mirimire', 0, 11, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(156, 'Pedregal', 'pedregal', 0, 11, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(157, 'Píritu Falcón', 'piritu-falcon', 0, 11, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(158, 'Pueblo Nuevo Falcón', 'pueblo-nuevo-falcon', 0, 11, '2020-05-04 07:25:14', '2020-05-04 07:25:14', NULL),
(159, 'Puerto Cumarebo', 'puerto-cumarebo', 0, 11, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(160, 'Punta Cardón', 'punta-cardon', 0, 11, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(161, 'Punto Fijo', 'punto-fijo', 0, 11, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(162, 'San Juan de Los Cayos', 'san-juan-de-los-cayos', 0, 11, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(163, 'San Luis', 'san-luis', 0, 11, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(164, 'Santa Ana Falcón', 'santa-ana-falcon', 0, 11, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(165, 'Santa Cruz De Bucaral', 'santa-cruz-de-bucaral', 0, 11, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(166, 'Tocopero', 'tocopero', 0, 11, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(167, 'Tocuyo de La Costa', 'tocuyo-de-la-costa', 0, 11, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(168, 'Tucacas', 'tucacas', 0, 11, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(169, 'Yaracal', 'yaracal', 0, 11, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(170, 'Altagracia de Orituco', 'altagracia-de-orituco', 0, 12, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(171, 'Cabruta', 'cabruta', 0, 12, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(172, 'Calabozo', 'calabozo', 0, 12, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(173, 'Camaguán', 'camaguan', 0, 12, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(174, 'Chaguaramas Guárico', 'chaguaramas-guarico', 0, 12, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(175, 'El Socorro', 'el-socorro', 0, 12, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(176, 'El Sombrero', 'el-sombrero', 0, 12, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(177, 'Las Mercedes de Los Llanos', 'las-mercedes-de-los-llanos', 0, 12, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(178, 'Lezama', 'lezama', 0, 12, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(179, 'Onoto', 'onoto', 0, 12, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(180, 'Ortíz', 'ortiz', 0, 12, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(181, 'San José de Guaribe', 'san-jose-de-guaribe', 0, 12, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(182, 'San Juan de Los Morros', 'san-juan-de-los-morros', 1, 12, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(183, 'San Rafael de Laya', 'san-rafael-de-laya', 0, 12, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(184, 'Santa María de Ipire', 'santa-maria-de-ipire', 0, 12, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(185, 'Tucupido', 'tucupido', 0, 12, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(186, 'Valle de La Pascua', 'valle-de-la-pascua', 0, 12, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(187, 'Zaraza', 'zaraza', 0, 12, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(188, 'Aguada Grande', 'aguada-grande', 0, 13, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(189, 'Atarigua', 'atarigua', 0, 13, '2020-05-04 07:25:15', '2020-05-04 07:25:15', NULL),
(190, 'Barquisimeto', 'barquisimeto', 1, 13, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(191, 'Bobare', 'bobare', 0, 13, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(192, 'Cabudare', 'cabudare', 0, 13, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(193, 'Carora', 'carora', 0, 13, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(194, 'Cubiro', 'cubiro', 0, 13, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(195, 'Cují', 'cuji', 0, 13, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(196, 'Duaca', 'duaca', 0, 13, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(197, 'El Manzano', 'el-manzano', 0, 13, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(198, 'El Tocuyo', 'el-tocuyo', 0, 13, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(199, 'Guaríco', 'guarico', 0, 13, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(200, 'Humocaro Alto', 'humocaro-alto', 0, 13, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(201, 'Humocaro Bajo', 'humocaro-bajo', 0, 13, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(202, 'La Miel', 'la-miel', 0, 13, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(203, 'Moroturo', 'moroturo', 0, 13, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(204, 'Quíbor', 'quibor', 0, 13, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(205, 'Río Claro', 'rio-claro', 0, 13, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(206, 'Sanare', 'sanare', 0, 13, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(207, 'Santa Inés', 'santa-ines', 0, 13, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(208, 'Sarare', 'sarare', 0, 13, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(209, 'Siquisique', 'siquisique', 0, 13, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(210, 'Tintorero', 'tintorero', 0, 13, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(211, 'Apartaderos Mérida', 'apartaderos-merida', 0, 14, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(212, 'Arapuey', 'arapuey', 0, 14, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(213, 'Bailadores', 'bailadores', 0, 14, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(214, 'Caja Seca', 'caja-seca', 0, 14, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(215, 'Canaguá', 'canagua', 0, 14, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(216, 'Chachopo', 'chachopo', 0, 14, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(217, 'Chiguara', 'chiguara', 0, 14, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(218, 'Ejido', 'ejido', 0, 14, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(219, 'El Vigía', 'el-vigia', 0, 14, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(220, 'La Azulita', 'la-azulita', 0, 14, '2020-05-04 07:25:16', '2020-05-04 07:25:16', NULL),
(221, 'La Playa', 'la-playa', 0, 14, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(222, 'Lagunillas Mérida', 'lagunillas-merida', 0, 14, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(223, 'Mérida', 'merida', 1, 14, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(224, 'Mesa de Bolívar', 'mesa-de-bolivar', 0, 14, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(225, 'Mucuchíes', 'mucuchies', 0, 14, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(226, 'Mucujepe', 'mucujepe', 0, 14, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(227, 'Mucuruba', 'mucuruba', 0, 14, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(228, 'Nueva Bolivia', 'nueva-bolivia', 0, 14, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(229, 'Palmarito', 'palmarito', 0, 14, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(230, 'Pueblo Llano', 'pueblo-llano', 0, 14, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(231, 'Santa Cruz de Mora', 'santa-cruz-de-mora', 0, 14, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(232, 'Santa Elena de Arenales', 'santa-elena-de-arenales', 0, 14, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(233, 'Santo Domingo', 'santo-domingo', 0, 14, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(234, 'Tabáy', 'tabay', 0, 14, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(235, 'Timotes', 'timotes', 0, 14, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(236, 'Torondoy', 'torondoy', 0, 14, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(237, 'Tovar', 'tovar', 0, 14, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(238, 'Tucani', 'tucani', 0, 14, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(239, 'Zea', 'zea', 0, 14, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(240, 'Araguita', 'araguita', 0, 15, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(241, 'Carrizal', 'carrizal', 0, 15, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(242, 'Caucagua', 'caucagua', 0, 15, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(243, 'Chaguaramas Miranda', 'chaguaramas-miranda', 0, 15, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(244, 'Charallave', 'charallave', 0, 15, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(245, 'Chirimena', 'chirimena', 0, 15, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(246, 'Chuspa', 'chuspa', 0, 15, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(247, 'Cúa', 'cua', 0, 15, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(248, 'Cupira', 'cupira', 0, 15, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(249, 'Curiepe', 'curiepe', 0, 15, '2020-05-04 07:25:17', '2020-05-04 07:25:17', NULL),
(250, 'El Guapo', 'el-guapo', 0, 15, '2020-05-04 07:25:18', '2020-05-04 07:25:18', NULL),
(251, 'El Jarillo', 'el-jarillo', 0, 15, '2020-05-04 07:25:18', '2020-05-04 07:25:18', NULL),
(252, 'Filas de Mariche', 'filas-de-mariche', 0, 15, '2020-05-04 07:25:18', '2020-05-04 07:25:18', NULL),
(253, 'Guarenas', 'guarenas', 0, 15, '2020-05-04 07:25:18', '2020-05-04 07:25:18', NULL),
(254, 'Guatire', 'guatire', 0, 15, '2020-05-04 07:25:18', '2020-05-04 07:25:18', NULL),
(255, 'Higuerote', 'higuerote', 0, 15, '2020-05-04 07:25:18', '2020-05-04 07:25:18', NULL),
(256, 'Los Anaucos', 'los-anaucos', 0, 15, '2020-05-04 07:25:18', '2020-05-04 07:25:18', NULL),
(257, 'Los Teques', 'los-teques', 1, 15, '2020-05-04 07:25:18', '2020-05-04 07:25:18', NULL),
(258, 'Ocumare del Tuy', 'ocumare-del-tuy', 0, 15, '2020-05-04 07:25:18', '2020-05-04 07:25:18', NULL),
(259, 'Panaquire', 'panaquire', 0, 15, '2020-05-04 07:25:18', '2020-05-04 07:25:18', NULL),
(260, 'Paracotos', 'paracotos', 0, 15, '2020-05-04 07:25:18', '2020-05-04 07:25:18', NULL),
(261, 'Río Chico', 'rio-chico', 0, 15, '2020-05-04 07:25:18', '2020-05-04 07:25:18', NULL),
(262, 'San Antonio de Los Altos', 'san-antonio-de-los-altos', 0, 15, '2020-05-04 07:25:18', '2020-05-04 07:25:18', NULL),
(263, 'San Diego de Los Altos', 'san-diego-de-los-altos', 0, 15, '2020-05-04 07:25:18', '2020-05-04 07:25:18', NULL),
(264, 'San Fernando del Guapo', 'san-fernando-del-guapo', 0, 15, '2020-05-04 07:25:18', '2020-05-04 07:25:18', NULL),
(265, 'San Francisco de Yare', 'san-francisco-de-yare', 0, 15, '2020-05-04 07:25:18', '2020-05-04 07:25:18', NULL),
(266, 'San José de Los Altos', 'san-jose-de-los-altos', 0, 15, '2020-05-04 07:25:18', '2020-05-04 07:25:18', NULL),
(267, 'San José de Río Chico', 'san-jose-de-rio-chico', 0, 15, '2020-05-04 07:25:18', '2020-05-04 07:25:18', NULL),
(268, 'San Pedro de Los Altos', 'san-pedro-de-los-altos', 0, 15, '2020-05-04 07:25:18', '2020-05-04 07:25:18', NULL),
(269, 'Santa Lucía', 'santa-lucia', 0, 15, '2020-05-04 07:25:18', '2020-05-04 07:25:18', NULL),
(270, 'Santa Teresa', 'santa-teresa', 0, 15, '2020-05-04 07:25:18', '2020-05-04 07:25:18', NULL),
(271, 'Tacarigua de La Laguna', 'tacarigua-de-la-laguna', 0, 15, '2020-05-04 07:25:18', '2020-05-04 07:25:18', NULL),
(272, 'Tacarigua de Mamporal', 'tacarigua-de-mamporal', 0, 15, '2020-05-04 07:25:18', '2020-05-04 07:25:18', NULL),
(273, 'Tácata', 'tacata', 0, 15, '2020-05-04 07:25:18', '2020-05-04 07:25:18', NULL),
(274, 'Turumo', 'turumo', 0, 15, '2020-05-04 07:25:18', '2020-05-04 07:25:18', NULL),
(275, 'Aguasay', 'aguasay', 0, 16, '2020-05-04 07:25:18', '2020-05-04 07:25:18', NULL),
(276, 'Aragua de Maturín', 'aragua-de-maturin', 0, 16, '2020-05-04 07:25:18', '2020-05-04 07:25:18', NULL),
(277, 'Barrancas del Orinoco', 'barrancas-del-orinoco', 0, 16, '2020-05-04 07:25:18', '2020-05-04 07:25:18', NULL),
(278, 'Caicara de Maturín', 'caicara-de-maturin', 0, 16, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(279, 'Caripe', 'caripe', 0, 16, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(280, 'Caripito', 'caripito', 0, 16, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(281, 'Chaguaramal', 'chaguaramal', 0, 16, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(282, 'Chaguaramas Monagas', 'chaguaramas-monagas', 0, 16, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(283, 'El Furrial', 'el-furrial', 0, 16, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(284, 'El Tejero', 'el-tejero', 0, 16, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(285, 'Jusepín', 'jusepin', 0, 16, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(286, 'La Toscana', 'la-toscana', 0, 16, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(287, 'Maturín', 'maturin', 1, 16, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(288, 'Miraflores', 'miraflores', 0, 16, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(289, 'Punta de Mata', 'punta-de-mata', 0, 16, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(290, 'Quiriquire', 'quiriquire', 0, 16, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(291, 'San Antonio de Maturín', 'san-antonio-de-maturin', 0, 16, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(292, 'San Vicente Monagas', 'san-vicente-monagas', 0, 16, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(293, 'Santa Bárbara', 'santa-barbara', 0, 16, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(294, 'Temblador', 'temblador', 0, 16, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(295, 'Teresen', 'teresen', 0, 16, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(296, 'Uracoa', 'uracoa', 0, 16, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(297, 'Altagracia', 'altagracia', 0, 17, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(298, 'Boca de Pozo', 'boca-de-pozo', 0, 17, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(299, 'Boca de Río', 'boca-de-rio', 0, 17, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(300, 'El Espinal', 'el-espinal', 0, 17, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(301, 'El Valle del Espíritu Santo', 'el-valle-del-espiritu-santo', 0, 17, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(302, 'El Yaque', 'el-yaque', 0, 17, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(303, 'Juangriego', 'juangriego', 0, 17, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(304, 'La Asunción', 'la-asuncion', 1, 17, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(305, 'La Guardia', 'la-guardia', 0, 17, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(306, 'Pampatar', 'pampatar', 0, 17, '2020-05-04 07:25:19', '2020-05-04 07:25:19', NULL),
(307, 'Porlamar', 'porlamar', 0, 17, '2020-05-04 07:25:20', '2020-05-04 07:25:20', NULL),
(308, 'Puerto Fermín', 'puerto-fermin', 0, 17, '2020-05-04 07:25:20', '2020-05-04 07:25:20', NULL),
(309, 'Punta de Piedras', 'punta-de-piedras', 0, 17, '2020-05-04 07:25:20', '2020-05-04 07:25:20', NULL),
(310, 'San Francisco de Macanao', 'san-francisco-de-macanao', 0, 17, '2020-05-04 07:25:20', '2020-05-04 07:25:20', NULL),
(311, 'San Juan Bautista', 'san-juan-bautista', 0, 17, '2020-05-04 07:25:20', '2020-05-04 07:25:20', NULL),
(312, 'San Pedro de Coche', 'san-pedro-de-coche', 0, 17, '2020-05-04 07:25:20', '2020-05-04 07:25:20', NULL),
(313, 'Santa Ana de Nueva Esparta', 'santa-ana-de-nueva-esparta', 0, 17, '2020-05-04 07:25:20', '2020-05-04 07:25:20', NULL),
(314, 'Villa Rosa', 'villa-rosa', 0, 17, '2020-05-04 07:25:20', '2020-05-04 07:25:20', NULL),
(315, 'Acarigua', 'acarigua', 0, 18, '2020-05-04 07:25:20', '2020-05-04 07:25:20', NULL),
(316, 'Agua Blanca', 'agua-blanca', 0, 18, '2020-05-04 07:25:20', '2020-05-04 07:25:20', NULL),
(317, 'Araure', 'araure', 0, 18, '2020-05-04 07:25:20', '2020-05-04 07:25:20', NULL),
(318, 'Biscucuy', 'biscucuy', 0, 18, '2020-05-04 07:25:20', '2020-05-04 07:25:20', NULL),
(319, 'Boconoito', 'boconoito', 0, 18, '2020-05-04 07:25:20', '2020-05-04 07:25:20', NULL),
(320, 'Campo Elías', 'campo-elias', 0, 18, '2020-05-04 07:25:20', '2020-05-04 07:25:20', NULL),
(321, 'Chabasquén', 'chabasquen', 0, 18, '2020-05-04 07:25:20', '2020-05-04 07:25:20', NULL),
(322, 'Guanare', 'guanare', 1, 18, '2020-05-04 07:25:20', '2020-05-04 07:25:20', NULL),
(323, 'Guanarito', 'guanarito', 0, 18, '2020-05-04 07:25:20', '2020-05-04 07:25:20', NULL),
(324, 'La Aparición', 'la-aparicion', 0, 18, '2020-05-04 07:25:20', '2020-05-04 07:25:20', NULL),
(325, 'La Misión', 'la-mision', 0, 18, '2020-05-04 07:25:20', '2020-05-04 07:25:20', NULL),
(326, 'Mesa de Cavacas', 'mesa-de-cavacas', 0, 18, '2020-05-04 07:25:20', '2020-05-04 07:25:20', NULL),
(327, 'Ospino', 'ospino', 0, 18, '2020-05-04 07:25:20', '2020-05-04 07:25:20', NULL),
(328, 'Papelón', 'papelon', 0, 18, '2020-05-04 07:25:20', '2020-05-04 07:25:20', NULL),
(329, 'Payara', 'payara', 0, 18, '2020-05-04 07:25:20', '2020-05-04 07:25:20', NULL),
(330, 'Pimpinela', 'pimpinela', 0, 18, '2020-05-04 07:25:20', '2020-05-04 07:25:20', NULL),
(331, 'Píritu de Portuguesa', 'piritu-de-portuguesa', 0, 18, '2020-05-04 07:25:20', '2020-05-04 07:25:20', NULL),
(332, 'San Rafael de Onoto', 'san-rafael-de-onoto', 0, 18, '2020-05-04 07:25:20', '2020-05-04 07:25:20', NULL),
(333, 'Santa Rosalía', 'santa-rosalia', 0, 18, '2020-05-04 07:25:20', '2020-05-04 07:25:20', NULL),
(334, 'Turén', 'turen', 0, 18, '2020-05-04 07:25:20', '2020-05-04 07:25:20', NULL),
(335, 'Altos de Sucre', 'altos-de-sucre', 0, 19, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(336, 'Araya', 'araya', 0, 19, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(337, 'Cariaco', 'cariaco', 0, 19, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(338, 'Carúpano', 'carupano', 0, 19, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(339, 'Casanay', 'casanay', 0, 19, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(340, 'Cumaná', 'cumana', 1, 19, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(341, 'Cumanacoa', 'cumanacoa', 0, 19, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(342, 'El Morro Puerto Santo', 'el-morro-puerto-santo', 0, 19, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(343, 'El Pilar', 'el-pilar', 0, 19, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(344, 'El Poblado', 'el-poblado', 0, 19, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(345, 'Guaca', 'guaca', 0, 19, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(346, 'Guiria', 'guiria', 0, 19, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(347, 'Irapa', 'irapa', 0, 19, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(348, 'Manicuare', 'manicuare', 0, 19, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(349, 'Mariguitar', 'mariguitar', 0, 19, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(350, 'Río Caribe', 'rio-caribe', 0, 19, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(351, 'San Antonio del Golfo', 'san-antonio-del-golfo', 0, 19, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(352, 'San José de Aerocuar', 'san-jose-de-aerocuar', 0, 19, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(353, 'San Vicente de Sucre', 'san-vicente-de-sucre', 0, 19, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(354, 'Santa Fe de Sucre', 'santa-fe-de-sucre', 0, 19, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(355, 'Tunapuy', 'tunapuy', 0, 19, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(356, 'Yaguaraparo', 'yaguaraparo', 0, 19, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(357, 'Yoco', 'yoco', 0, 19, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(358, 'Abejales', 'abejales', 0, 20, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(359, 'Borota', 'borota', 0, 20, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(360, 'Bramon', 'bramon', 0, 20, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(361, 'Capacho', 'capacho', 0, 20, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(362, 'Colón', 'colon', 0, 20, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(363, 'Coloncito', 'coloncito', 0, 20, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(364, 'Cordero', 'cordero', 0, 20, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(365, 'El Cobre', 'el-cobre', 0, 20, '2020-05-04 07:25:21', '2020-05-04 07:25:21', NULL),
(366, 'El Pinal', 'el-pinal', 0, 20, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(367, 'Independencia', 'independencia', 0, 20, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(368, 'La Fría', 'la-fria', 0, 20, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(369, 'La Grita', 'la-grita', 0, 20, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(370, 'La Pedrera', 'la-pedrera', 0, 20, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(371, 'La Tendida', 'la-tendida', 0, 20, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(372, 'Las Delicias', 'las-delicias', 0, 20, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(373, 'Las Hernández', 'las-hernandez', 0, 20, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(374, 'Lobatera', 'lobatera', 0, 20, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(375, 'Michelena', 'michelena', 0, 20, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(376, 'Palmira', 'palmira', 0, 20, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(377, 'Pregonero', 'pregonero', 0, 20, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(378, 'Queniquea', 'queniquea', 0, 20, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(379, 'Rubio', 'rubio', 0, 20, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(380, 'San Antonio del Tachira', 'san-antonio-del-tachira', 0, 20, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(381, 'San Cristobal', 'san-cristobal', 1, 20, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(382, 'San José de Bolívar', 'san-jose-de-bolivar', 0, 20, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(383, 'San Josecito', 'san-josecito', 0, 20, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(384, 'San Pedro del Río', 'san-pedro-del-rio', 0, 20, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(385, 'Santa Ana Táchira', 'santa-ana-tachira', 0, 20, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(386, 'Seboruco', 'seboruco', 0, 20, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(387, 'Táriba', 'tariba', 0, 20, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(388, 'Umuquena', 'umuquena', 0, 20, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(389, 'Ureña', 'urena', 0, 20, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(390, 'Batatal', 'batatal', 0, 21, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(391, 'Betijoque', 'betijoque', 0, 21, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(392, 'Boconó', 'bocono', 0, 21, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(393, 'Carache', 'carache', 0, 21, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(394, 'Chejende', 'chejende', 0, 21, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(395, 'Cuicas', 'cuicas', 0, 21, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(396, 'El Dividive', 'el-dividive', 0, 21, '2020-05-04 07:25:22', '2020-05-04 07:25:22', NULL),
(397, 'El Jaguito', 'el-jaguito', 0, 21, '2020-05-04 07:25:23', '2020-05-04 07:25:23', NULL),
(398, 'Escuque', 'escuque', 0, 21, '2020-05-04 07:25:23', '2020-05-04 07:25:23', NULL),
(399, 'Isnotú', 'isnotu', 0, 21, '2020-05-04 07:25:23', '2020-05-04 07:25:23', NULL),
(400, 'Jajó', 'jajo', 0, 21, '2020-05-04 07:25:23', '2020-05-04 07:25:23', NULL),
(401, 'La Ceiba', 'la-ceiba', 0, 21, '2020-05-04 07:25:23', '2020-05-04 07:25:23', NULL),
(402, 'La Concepción de Trujllo', 'la-concepcion-de-trujllo', 0, 21, '2020-05-04 07:25:23', '2020-05-04 07:25:23', NULL),
(403, 'La Mesa de Esnujaque', 'la-mesa-de-esnujaque', 0, 21, '2020-05-04 07:25:23', '2020-05-04 07:25:23', NULL),
(404, 'La Puerta', 'la-puerta', 0, 21, '2020-05-04 07:25:23', '2020-05-04 07:25:23', NULL),
(405, 'La Quebrada', 'la-quebrada', 0, 21, '2020-05-04 07:25:23', '2020-05-04 07:25:23', NULL),
(406, 'Mendoza Fría', 'mendoza-fria', 0, 21, '2020-05-04 07:25:23', '2020-05-04 07:25:23', NULL),
(407, 'Meseta de Chimpire', 'meseta-de-chimpire', 0, 21, '2020-05-04 07:25:23', '2020-05-04 07:25:23', NULL),
(408, 'Monay', 'monay', 0, 21, '2020-05-04 07:25:23', '2020-05-04 07:25:23', NULL),
(409, 'Motatán', 'motatan', 0, 21, '2020-05-04 07:25:23', '2020-05-04 07:25:23', NULL),
(410, 'Pampán', 'pampan', 0, 21, '2020-05-04 07:25:23', '2020-05-04 07:25:23', NULL),
(411, 'Pampanito', 'pampanito', 0, 21, '2020-05-04 07:25:23', '2020-05-04 07:25:23', NULL),
(412, 'Sabana de Mendoza', 'sabana-de-mendoza', 0, 21, '2020-05-04 07:25:23', '2020-05-04 07:25:23', NULL),
(413, 'San Lázaro', 'san-lazaro', 0, 21, '2020-05-04 07:25:23', '2020-05-04 07:25:23', NULL),
(414, 'Santa Ana de Trujillo', 'santa-ana-de-trujillo', 0, 21, '2020-05-04 07:25:23', '2020-05-04 07:25:23', NULL),
(415, 'Tostós', 'tostos', 0, 21, '2020-05-04 07:25:23', '2020-05-04 07:25:23', NULL),
(416, 'Trujillo', 'trujillo', 1, 21, '2020-05-04 07:25:23', '2020-05-04 07:25:23', NULL),
(417, 'Valera', 'valera', 0, 21, '2020-05-04 07:25:23', '2020-05-04 07:25:23', NULL),
(418, 'Carayaca', 'carayaca', 0, 22, '2020-05-04 07:25:23', '2020-05-04 07:25:23', NULL),
(419, 'Litoral', 'litoral', 0, 22, '2020-05-04 07:25:23', '2020-05-04 07:25:23', NULL),
(420, 'La Guaira', 'la-guaira', 1, 22, '2020-05-04 07:25:24', '2020-05-04 07:25:24', NULL),
(421, 'Catia La Mar', 'catia-la-mar', 0, 22, '2020-05-04 07:25:24', '2020-05-04 07:25:24', NULL),
(422, 'Macuto', 'macuto', 0, 22, '2020-05-04 07:25:24', '2020-05-04 07:25:24', NULL),
(423, 'Naiguatá', 'naiguata', 0, 22, '2020-05-04 07:25:24', '2020-05-04 07:25:24', NULL),
(424, 'Aroa', 'aroa', 0, 23, '2020-05-04 07:25:24', '2020-05-04 07:25:24', NULL),
(425, 'Boraure', 'boraure', 0, 23, '2020-05-04 07:25:24', '2020-05-04 07:25:24', NULL),
(426, 'Campo Elías de Yaracuy', 'campo-elias-de-yaracuy', 0, 23, '2020-05-04 07:25:24', '2020-05-04 07:25:24', NULL),
(427, 'Chivacoa', 'chivacoa', 0, 23, '2020-05-04 07:25:24', '2020-05-04 07:25:24', NULL),
(428, 'Cocorote', 'cocorote', 0, 23, '2020-05-04 07:25:24', '2020-05-04 07:25:24', NULL),
(429, 'Farriar', 'farriar', 0, 23, '2020-05-04 07:25:24', '2020-05-04 07:25:24', NULL),
(430, 'Guama', 'guama', 0, 23, '2020-05-04 07:25:24', '2020-05-04 07:25:24', NULL),
(431, 'Marín', 'marin', 0, 23, '2020-05-04 07:25:24', '2020-05-04 07:25:24', NULL),
(432, 'Nirgua', 'nirgua', 0, 23, '2020-05-04 07:25:24', '2020-05-04 07:25:24', NULL),
(433, 'Sabana de Parra', 'sabana-de-parra', 0, 23, '2020-05-04 07:25:24', '2020-05-04 07:25:24', NULL),
(434, 'Salom', 'salom', 0, 23, '2020-05-04 07:25:24', '2020-05-04 07:25:24', NULL),
(435, 'San Felipe', 'san-felipe', 1, 23, '2020-05-04 07:25:24', '2020-05-04 07:25:24', NULL),
(436, 'San Pablo de Yaracuy', 'san-pablo-de-yaracuy', 0, 23, '2020-05-04 07:25:24', '2020-05-04 07:25:24', NULL),
(437, 'Urachiche', 'urachiche', 0, 23, '2020-05-04 07:25:24', '2020-05-04 07:25:24', NULL),
(438, 'Yaritagua', 'yaritagua', 0, 23, '2020-05-04 07:25:24', '2020-05-04 07:25:24', NULL),
(439, 'Yumare', 'yumare', 0, 23, '2020-05-04 07:25:24', '2020-05-04 07:25:24', NULL),
(440, 'Bachaquero', 'bachaquero', 0, 24, '2020-05-04 07:25:24', '2020-05-04 07:25:24', NULL),
(441, 'Bobures', 'bobures', 0, 24, '2020-05-04 07:25:25', '2020-05-04 07:25:25', NULL),
(442, 'Cabimas', 'cabimas', 0, 24, '2020-05-04 07:25:25', '2020-05-04 07:25:25', NULL),
(443, 'Campo Concepción', 'campo-concepcion', 0, 24, '2020-05-04 07:25:25', '2020-05-04 07:25:25', NULL),
(444, 'Campo Mara', 'campo-mara', 0, 24, '2020-05-04 07:25:25', '2020-05-04 07:25:25', NULL),
(445, 'Campo Rojo', 'campo-rojo', 0, 24, '2020-05-04 07:25:25', '2020-05-04 07:25:25', NULL),
(446, 'Carrasquero', 'carrasquero', 0, 24, '2020-05-04 07:25:25', '2020-05-04 07:25:25', NULL),
(447, 'Casigua', 'casigua', 0, 24, '2020-05-04 07:25:25', '2020-05-04 07:25:25', NULL),
(448, 'Chiquinquirá', 'chiquinquira', 0, 24, '2020-05-04 07:25:25', '2020-05-04 07:25:25', NULL),
(449, 'Ciudad Ojeda', 'ciudad-ojeda', 0, 24, '2020-05-04 07:25:25', '2020-05-04 07:25:25', NULL),
(450, 'El Batey', 'el-batey', 0, 24, '2020-05-04 07:25:25', '2020-05-04 07:25:25', NULL),
(451, 'El Carmelo', 'el-carmelo', 0, 24, '2020-05-04 07:25:25', '2020-05-04 07:25:25', NULL),
(452, 'El Chivo', 'el-chivo', 0, 24, '2020-05-04 07:25:25', '2020-05-04 07:25:25', NULL),
(453, 'El Guayabo', 'el-guayabo', 0, 24, '2020-05-04 07:25:25', '2020-05-04 07:25:25', NULL),
(454, 'El Mene', 'el-mene', 0, 24, '2020-05-04 07:25:25', '2020-05-04 07:25:25', NULL),
(455, 'El Venado', 'el-venado', 0, 24, '2020-05-04 07:25:25', '2020-05-04 07:25:25', NULL),
(456, 'Encontrados', 'encontrados', 0, 24, '2020-05-04 07:25:25', '2020-05-04 07:25:25', NULL),
(457, 'Gibraltar', 'gibraltar', 0, 24, '2020-05-04 07:25:25', '2020-05-04 07:25:25', NULL),
(458, 'Isla de Toas', 'isla-de-toas', 0, 24, '2020-05-04 07:25:25', '2020-05-04 07:25:25', NULL),
(459, 'La Concepción del Zulia', 'la-concepcion-del-zulia', 0, 24, '2020-05-04 07:25:25', '2020-05-04 07:25:25', NULL),
(460, 'La Paz', 'la-paz', 0, 24, '2020-05-04 07:25:25', '2020-05-04 07:25:25', NULL),
(461, 'La Sierrita', 'la-sierrita', 0, 24, '2020-05-04 07:25:25', '2020-05-04 07:25:25', NULL),
(462, 'Lagunillas del Zulia', 'lagunillas-del-zulia', 0, 24, '2020-05-04 07:25:25', '2020-05-04 07:25:25', NULL),
(463, 'Las Piedras de Perijá', 'las-piedras-de-perija', 0, 24, '2020-05-04 07:25:25', '2020-05-04 07:25:25', NULL),
(464, 'Los Cortijos', 'los-cortijos', 0, 24, '2020-05-04 07:25:25', '2020-05-04 07:25:25', NULL),
(465, 'Machiques', 'machiques', 0, 24, '2020-05-04 07:25:25', '2020-05-04 07:25:25', NULL),
(466, 'Maracaibo', 'maracaibo', 1, 24, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(467, 'Mene Grande', 'mene-grande', 0, 24, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(468, 'Palmarejo', 'palmarejo', 0, 24, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(469, 'Paraguaipoa', 'paraguaipoa', 0, 24, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(470, 'Potrerito', 'potrerito', 0, 24, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(471, 'Pueblo Nuevo del Zulia', 'pueblo-nuevo-del-zulia', 0, 24, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(472, 'Puertos de Altagracia', 'puertos-de-altagracia', 0, 24, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(473, 'Punta Gorda', 'punta-gorda', 0, 24, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(474, 'Sabaneta de Palma', 'sabaneta-de-palma', 0, 24, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(475, 'San Francisco', 'san-francisco', 0, 24, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(476, 'San José de Perijá', 'san-jose-de-perija', 0, 24, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(477, 'San Rafael del Moján', 'san-rafael-del-mojan', 0, 24, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(478, 'San Timoteo', 'san-timoteo', 0, 24, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(479, 'Santa Bárbara Del Zulia', 'santa-barbara-del-zulia', 0, 24, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(480, 'Santa Cruz de Mara', 'santa-cruz-de-mara', 0, 24, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(481, 'Santa Cruz del Zulia', 'santa-cruz-del-zulia', 0, 24, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(482, 'Santa Rita', 'santa-rita', 0, 24, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(483, 'Sinamaica', 'sinamaica', 0, 24, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(484, 'Tamare', 'tamare', 0, 24, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(485, 'Tía Juana', 'tia-juana', 0, 24, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(486, 'Villa del Rosario', 'villa-del-rosario', 0, 24, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(487, 'Archipiélago Los Roques', 'archipielago-los-roques', 0, 25, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(488, 'Archipiélago Los Monjes', 'archipielago-los-monjes', 0, 25, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(489, 'Isla La Tortuga y Cayos adyacentes', 'isla-la-tortuga-y-cayos-adyacentes', 0, 25, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(490, 'Isla La Sola', 'isla-la-sola', 0, 25, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(491, 'Islas Los Testigos', 'islas-los-testigos', 0, 25, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(492, 'Islas Los Frailes', 'islas-los-frailes', 0, 25, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(493, 'Isla La Orchila', 'isla-la-orchila', 0, 25, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(494, 'Archipiélago Las Aves', 'archipielago-las-aves', 0, 25, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(495, 'Isla de Aves', 'isla-de-aves', 0, 25, '2020-05-04 07:25:26', '2020-05-04 07:25:26', NULL),
(496, 'Isla La Blanquilla', 'isla-la-blanquilla', 0, 25, '2020-05-04 07:25:27', '2020-05-04 07:25:27', NULL),
(497, 'Isla de Patos', 'isla-de-patos', 0, 25, '2020-05-04 07:25:27', '2020-05-04 07:25:27', NULL),
(498, 'Islas Los Hermanos', 'islas-los-hermanos', 0, 25, '2020-05-04 07:25:27', '2020-05-04 07:25:27', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contracts`
--

CREATE TABLE `contracts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `contract_date` date NOT NULL,
  `salary` double(10,2) UNSIGNED NOT NULL,
  `observations` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `heritage_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `countries`
--

CREATE TABLE `countries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `countries`
--

INSERT INTO `countries` (`id`, `country`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Venezuela', 'ven', '2020-05-04 07:25:07', '2020-05-04 07:25:07', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dni` int(11) NOT NULL,
  `phone` int(11) NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `customer_type_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customer_types`
--

CREATE TABLE `customer_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `employees`
--

CREATE TABLE `employees` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dni` int(11) NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birth` date NOT NULL,
  `status` enum('nuevo','re-contrato','renuncio','no admitible') COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `heritages`
--

CREATE TABLE `heritages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zip_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `headquarter` int(11) NOT NULL DEFAULT '0' COMMENT '1. Sede Principal, 0. sucursal',
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `layout_id` bigint(20) UNSIGNED NOT NULL,
  `city_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imageable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imageable_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventories`
--

CREATE TABLE `inventories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `arrival` int(11) NOT NULL COMMENT 'El dia de entrada del producto a la tienda',
  `due_date` int(11) NOT NULL COMMENT 'Fecha de vencimiento del producto',
  `product_presentation` enum('kilo','gramo','litro','mililitros','unidad') COLLATE utf8mb4_unicode_ci NOT NULL,
  `wholesale_quantity` bigint(20) UNSIGNED NOT NULL COMMENT 'cantidad total que contiene un paquete',
  `purchase_price` double(10,2) NOT NULL COMMENT 'Registramos el precio de compra para hacer notas de creditos por ejemplo',
  `status` enum('disponible','no disponible') COLLATE utf8mb4_unicode_ci NOT NULL,
  `observation` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `heritage_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `layouts`
--

CREATE TABLE `layouts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `layout` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(67, '2014_10_12_000000_create_users_table', 1),
(68, '2014_10_12_100000_create_password_resets_table', 1),
(69, '2015_01_20_084450_create_roles_table', 1),
(70, '2015_01_20_084525_create_role_user_table', 1),
(71, '2015_01_24_080208_create_permissions_table', 1),
(72, '2015_01_24_080433_create_permission_role_table', 1),
(73, '2015_12_04_003040_add_special_role_column', 1),
(74, '2015_12_04_003050_add_unique_name_column', 1),
(75, '2017_10_17_170735_create_permission_user_table', 1),
(76, '2020_02_13_163336_create_layouts_table', 1),
(77, '2020_02_13_181848_create_countries_table', 1),
(78, '2020_02_13_182219_create_states_table', 1),
(79, '2020_02_13_182310_create_cities_table', 1),
(80, '2020_02_13_183336_create_heritages_table', 1),
(81, '2020_02_13_184501_create_employees_table', 1),
(82, '2020_02_13_184545_create_contracts_table', 1),
(83, '2020_02_13_184850_create_suppliers_table', 1),
(84, '2020_02_13_184852_create_categories_table', 1),
(85, '2020_02_13_191724_create_brands_table', 1),
(86, '2020_02_13_194725_create_product_types_table', 1),
(87, '2020_02_13_194726_create_products_table', 1),
(88, '2020_02_13_194727_create_promotion_types_table', 1),
(89, '2020_02_13_194737_create_promotions_table', 1),
(90, '2020_02_13_195454_create_inventories_table', 1),
(91, '2020_02_13_195455_create_sales_table', 1),
(92, '2020_02_13_195635_create_customer_types_table', 1),
(93, '2020_02_13_195835_create_customers_table', 1),
(94, '2020_02_28_015738_create_services_table', 1),
(95, '2020_02_28_023153_create_billings_table', 1),
(96, '2020_02_28_024900_create_billing_details_table', 1),
(97, '2020_03_21_204930_create_jobs_table', 1),
(98, '2020_03_28_132148_create_images_table', 1),
(99, '2020_05_01_235218_create_plans_table', 1),
(100, '2020_05_01_235613_create_subscriptions_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission_role`
--

CREATE TABLE `permission_role` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission_user`
--

CREATE TABLE `permission_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plans`
--

CREATE TABLE `plans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `plan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_frame` enum('15D','1M','3M','6M','1A','5A') COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(10,2) NOT NULL,
  `product_presentation` enum('activo','inactivo','suspendido','eliminado','limitado') COLLATE utf8mb4_unicode_ci NOT NULL,
  `promotion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `percentage` tinyint(4) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `brand_id` bigint(20) UNSIGNED NOT NULL,
  `product_type_id` bigint(20) UNSIGNED NOT NULL,
  `supplier_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product_types`
--

CREATE TABLE `product_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promotions`
--

CREATE TABLE `promotions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `promotion` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('activo','agotado','no disponible','finalizado') COLLATE utf8mb4_unicode_ci NOT NULL,
  `promotion_type_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promotion_types`
--

CREATE TABLE `promotion_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `promotion` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Entre los tipos pueden ser por defecto: reembolsos, premios, cupones, sorteo, concurso, descuentos, ofertas.',
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `special` enum('all-access','no-access') COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_user`
--

CREATE TABLE `role_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sales`
--

CREATE TABLE `sales` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_sale` enum('mayor','detal') COLLATE utf8mb4_unicode_ci NOT NULL,
  `condition` enum('nuevo','usado','deteriorado') COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(10,2) NOT NULL,
  `inventory_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `services`
--

CREATE TABLE `services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `service` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_service` enum('remunerado','gratis') COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(10,2) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `states`
--

CREATE TABLE `states` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `states`
--

INSERT INTO `states` (`id`, `state`, `slug`, `country_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Amazonas', 'amazonas', 1, '2020-05-04 07:25:07', '2020-05-04 07:25:07', NULL),
(2, 'Anzoátegui', 'anzoategui', 1, '2020-05-04 07:25:07', '2020-05-04 07:25:07', NULL),
(3, 'Apure', 'apure', 1, '2020-05-04 07:25:07', '2020-05-04 07:25:07', NULL),
(4, 'Aragua', 'aragua', 1, '2020-05-04 07:25:07', '2020-05-04 07:25:07', NULL),
(5, 'Barinas', 'barinas', 1, '2020-05-04 07:25:07', '2020-05-04 07:25:07', NULL),
(6, 'Bolívar', 'bolivar', 1, '2020-05-04 07:25:07', '2020-05-04 07:25:07', NULL),
(7, 'Carabobo', 'carabobo', 1, '2020-05-04 07:25:07', '2020-05-04 07:25:07', NULL),
(8, 'Cojedes', 'cojedes', 1, '2020-05-04 07:25:07', '2020-05-04 07:25:07', NULL),
(9, 'Delta Amacuro', 'delta-amacuro', 1, '2020-05-04 07:25:08', '2020-05-04 07:25:08', NULL),
(10, 'Distrito Capital', 'distrito-capital', 1, '2020-05-04 07:25:08', '2020-05-04 07:25:08', NULL),
(11, 'Falcón', 'falcon', 1, '2020-05-04 07:25:08', '2020-05-04 07:25:08', NULL),
(12, 'Guárico', 'guarico', 1, '2020-05-04 07:25:08', '2020-05-04 07:25:08', NULL),
(13, 'Lara', 'lara', 1, '2020-05-04 07:25:08', '2020-05-04 07:25:08', NULL),
(14, 'Mérida', 'merida', 1, '2020-05-04 07:25:08', '2020-05-04 07:25:08', NULL),
(15, 'Miranda', 'miranda', 1, '2020-05-04 07:25:08', '2020-05-04 07:25:08', NULL),
(16, 'Monagas', 'monagas', 1, '2020-05-04 07:25:08', '2020-05-04 07:25:08', NULL),
(17, 'Nueva Esparta', 'nueva-esparta', 1, '2020-05-04 07:25:08', '2020-05-04 07:25:08', NULL),
(18, 'Portuguesa', 'portuguesa', 1, '2020-05-04 07:25:08', '2020-05-04 07:25:08', NULL),
(19, 'Sucre', 'sucre', 1, '2020-05-04 07:25:08', '2020-05-04 07:25:08', NULL),
(20, 'Táchira', 'tachira', 1, '2020-05-04 07:25:08', '2020-05-04 07:25:08', NULL),
(21, 'Trujillo', 'trujillo', 1, '2020-05-04 07:25:08', '2020-05-04 07:25:08', NULL),
(22, 'Vargas', 'vargas', 1, '2020-05-04 07:25:08', '2020-05-04 07:25:08', NULL),
(23, 'Yaracuy', 'yaracuy', 1, '2020-05-04 07:25:08', '2020-05-04 07:25:08', NULL),
(24, 'Zulia', 'zulia', 1, '2020-05-04 07:25:08', '2020-05-04 07:25:08', NULL),
(25, 'Dependencias Federales', 'dependencias-federales', 1, '2020-05-04 07:25:08', '2020-05-04 07:25:08', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `price` double(10,2) NOT NULL,
  `status` enum('activo','suspendido','eliminado','renovado') COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `plan_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `suppliers`
--

CREATE TABLE `suppliers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `suppliers` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zip_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `billings`
--
ALTER TABLE `billings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `billings_employee_id_foreign` (`employee_id`),
  ADD KEY `billings_customer_id_foreign` (`customer_id`);

--
-- Indices de la tabla `billing_details`
--
ALTER TABLE `billing_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `billing_details_billing_id_foreign` (`billing_id`);

--
-- Indices de la tabla `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cities_state_id_foreign` (`state_id`);

--
-- Indices de la tabla `contracts`
--
ALTER TABLE `contracts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contracts_employee_id_foreign` (`employee_id`),
  ADD KEY `contracts_heritage_id_foreign` (`heritage_id`);

--
-- Indices de la tabla `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_dni_unique` (`dni`),
  ADD KEY `customers_user_id_foreign` (`user_id`),
  ADD KEY `customers_customer_type_id_foreign` (`customer_type_id`);

--
-- Indices de la tabla `customer_types`
--
ALTER TABLE `customer_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employees_dni_unique` (`dni`),
  ADD KEY `employees_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `heritages`
--
ALTER TABLE `heritages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `heritages_user_id_foreign` (`user_id`),
  ADD KEY `heritages_layout_id_foreign` (`layout_id`),
  ADD KEY `heritages_city_id_foreign` (`city_id`);

--
-- Indices de la tabla `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `images_imageable_type_imageable_id_index` (`imageable_type`,`imageable_id`);

--
-- Indices de la tabla `inventories`
--
ALTER TABLE `inventories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `inventories_product_id_foreign` (`product_id`),
  ADD KEY `inventories_heritage_id_foreign` (`heritage_id`);

--
-- Indices de la tabla `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indices de la tabla `layouts`
--
ALTER TABLE `layouts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_slug_unique` (`slug`);

--
-- Indices de la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indices de la tabla `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_user_permission_id_index` (`permission_id`),
  ADD KEY `permission_user_user_id_index` (`user_id`);

--
-- Indices de la tabla `plans`
--
ALTER TABLE `plans`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_category_id_foreign` (`category_id`),
  ADD KEY `products_brand_id_foreign` (`brand_id`),
  ADD KEY `products_product_type_id_foreign` (`product_type_id`),
  ADD KEY `products_supplier_id_foreign` (`supplier_id`);

--
-- Indices de la tabla `product_types`
--
ALTER TABLE `product_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `promotions`
--
ALTER TABLE `promotions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `promotions_promotion_type_id_foreign` (`promotion_type_id`);

--
-- Indices de la tabla `promotion_types`
--
ALTER TABLE `promotion_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indices de la tabla `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_role_id_index` (`role_id`),
  ADD KEY `role_user_user_id_index` (`user_id`);

--
-- Indices de la tabla `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sales_inventory_id_foreign` (`inventory_id`);

--
-- Indices de la tabla `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`),
  ADD KEY `states_country_id_foreign` (`country_id`);

--
-- Indices de la tabla `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subscriptions_user_id_foreign` (`user_id`),
  ADD KEY `subscriptions_plan_id_foreign` (`plan_id`);

--
-- Indices de la tabla `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_name_unique` (`name`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `billings`
--
ALTER TABLE `billings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `billing_details`
--
ALTER TABLE `billing_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `brands`
--
ALTER TABLE `brands`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cities`
--
ALTER TABLE `cities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=499;

--
-- AUTO_INCREMENT de la tabla `contracts`
--
ALTER TABLE `contracts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `countries`
--
ALTER TABLE `countries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `customer_types`
--
ALTER TABLE `customer_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `employees`
--
ALTER TABLE `employees`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `heritages`
--
ALTER TABLE `heritages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `inventories`
--
ALTER TABLE `inventories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `layouts`
--
ALTER TABLE `layouts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `permission_user`
--
ALTER TABLE `permission_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `plans`
--
ALTER TABLE `plans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `product_types`
--
ALTER TABLE `product_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `promotions`
--
ALTER TABLE `promotions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `promotion_types`
--
ALTER TABLE `promotion_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sales`
--
ALTER TABLE `sales`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `states`
--
ALTER TABLE `states`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `billings`
--
ALTER TABLE `billings`
  ADD CONSTRAINT `billings_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `billings_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `billing_details`
--
ALTER TABLE `billing_details`
  ADD CONSTRAINT `billing_details_billing_id_foreign` FOREIGN KEY (`billing_id`) REFERENCES `billings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `contracts`
--
ALTER TABLE `contracts`
  ADD CONSTRAINT `contracts_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `contracts_heritage_id_foreign` FOREIGN KEY (`heritage_id`) REFERENCES `heritages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `customers_customer_type_id_foreign` FOREIGN KEY (`customer_type_id`) REFERENCES `customer_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `customers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `heritages`
--
ALTER TABLE `heritages`
  ADD CONSTRAINT `heritages_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `heritages_layout_id_foreign` FOREIGN KEY (`layout_id`) REFERENCES `layouts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `heritages_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `inventories`
--
ALTER TABLE `inventories`
  ADD CONSTRAINT `inventories_heritage_id_foreign` FOREIGN KEY (`heritage_id`) REFERENCES `heritages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `inventories_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_product_type_id_foreign` FOREIGN KEY (`product_type_id`) REFERENCES `product_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `promotions`
--
ALTER TABLE `promotions`
  ADD CONSTRAINT `promotions_promotion_type_id_foreign` FOREIGN KEY (`promotion_type_id`) REFERENCES `promotion_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `sales`
--
ALTER TABLE `sales`
  ADD CONSTRAINT `sales_inventory_id_foreign` FOREIGN KEY (`inventory_id`) REFERENCES `inventories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `states`
--
ALTER TABLE `states`
  ADD CONSTRAINT `states_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD CONSTRAINT `subscriptions_plan_id_foreign` FOREIGN KEY (`plan_id`) REFERENCES `plans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `subscriptions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

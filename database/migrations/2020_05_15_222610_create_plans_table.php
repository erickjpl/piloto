<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->id();
            $table->string('plan');
            $table->string('slug')->unique();
            $table->string('description');
            $table->enum('time_frame', ['15D','1M','3M','6M','1A','5A']);
            $table->double('price', 10 , 2);
            $table->enum('product_presentation', ['activo','inactivo','suspendido','eliminado','limitado']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}

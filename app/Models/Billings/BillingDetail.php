<?php

namespace App\Models\Billings;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class BillingDetail
 * @package App\Models\Billings
 * @version May 16, 2020, 6:45 pm UTC
 *
 * @property \App\Models\Billings\Billing $billing
 * @property \App\Models\Billings\Promotion $promotion
 * @property integer $quantity
 * @property number $tax
 * @property number $price
 * @property integer $promotion_id
 * @property integer $billing_id
 */
class BillingDetail extends Model
{
    use SoftDeletes;

    public $table = 'billing_details';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'quantity',
        'tax',
        'price',
        'promotion_id',
        'billing_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'quantity' => 'integer',
        'tax' => 'float',
        'price' => 'float',
        'promotion_id' => 'integer',
        'billing_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'quantity' => 'required',
        'tax' => 'required',
        'price' => 'required',
        'billing_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function billing()
    {
        return $this->belongsTo(\App\Models\Billings\Billing::class, 'billing_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function promotion()
    {
        return $this->belongsTo(\App\Models\Billings\Promotion::class, 'promotion_id');
    }
}

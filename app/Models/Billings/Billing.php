<?php

namespace App\Models\Billings;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Billing
 * @package App\Models\Billings
 * @version May 16, 2020, 6:45 pm UTC
 *
 * @property \App\Models\Billings\Customer $customer
 * @property \App\Models\Billings\Employee $employee
 * @property \App\Models\Billings\Promotion $promotion
 * @property \Illuminate\Database\Eloquent\Collection $billingDetails
 * @property string $way_paying
 * @property integer $employee_id
 * @property integer $customer_id
 * @property integer $promotion_id
 */
class Billing extends Model
{
    use SoftDeletes;

    public $table = 'billings';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'way_paying',
        'employee_id',
        'customer_id',
        'promotion_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'way_paying' => 'string',
        'employee_id' => 'integer',
        'customer_id' => 'integer',
        'promotion_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'way_paying' => 'required',
        'employee_id' => 'required',
        'customer_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function customer()
    {
        return $this->belongsTo(\App\Models\Billings\Customer::class, 'customer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function employee()
    {
        return $this->belongsTo(\App\Models\Billings\Employee::class, 'employee_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function promotion()
    {
        return $this->belongsTo(\App\Models\Billings\Promotion::class, 'promotion_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function billingDetails()
    {
        return $this->hasMany(\App\Models\Billings\BillingDetail::class, 'billing_id');
    }
}

<?php

namespace App\Models\Customers;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CustomerType
 * @package App\Models\Customers
 * @version May 16, 2020, 6:45 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $customers
 * @property string $type
 * @property string $slug
 * @property string $description
 */
class CustomerType extends Model
{
    use SoftDeletes;

    public $table = 'customer_types';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'type',
        'slug',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'type' => 'string',
        'slug' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'type' => 'required',
        'slug' => 'required',
        'description' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function customers()
    {
        return $this->hasMany(\App\Models\Customers\Customer::class, 'customer_type_id');
    }
}

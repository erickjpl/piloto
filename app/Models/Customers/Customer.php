<?php

namespace App\Models\Customers;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Customer
 * @package App\Models\Customers
 * @version May 16, 2020, 6:45 pm UTC
 *
 * @property \App\Models\Customers\CustomerType $customerType
 * @property \App\Models\Customers\User $user
 * @property \Illuminate\Database\Eloquent\Collection $billings
 * @property string $name
 * @property string $lastname
 * @property integer $dni
 * @property integer $phone
 * @property string $email
 * @property string $address
 * @property integer $user_id
 * @property integer $customer_type_id
 */
class Customer extends Model
{
    use SoftDeletes;

    public $table = 'customers';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'lastname',
        'dni',
        'phone',
        'email',
        'address',
        'user_id',
        'customer_type_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'lastname' => 'string',
        'dni' => 'integer',
        'phone' => 'integer',
        'email' => 'string',
        'address' => 'string',
        'user_id' => 'integer',
        'customer_type_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'lastname' => 'required',
        'dni' => 'required',
        'phone' => 'required',
        'email' => 'required',
        'address' => 'required',
        'user_id' => 'required',
        'customer_type_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function customerType()
    {
        return $this->belongsTo(\App\Models\Customers\CustomerType::class, 'customer_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\Customers\User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function billings()
    {
        return $this->hasMany(\App\Models\Customers\Billing::class, 'customer_id');
    }
}

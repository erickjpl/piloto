<?php

namespace App\Models\Profile;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Employee
 * @package App\Models\Profile
 * @version May 16, 2020, 6:45 pm UTC
 *
 * @property \App\Models\Profile\User $user
 * @property \Illuminate\Database\Eloquent\Collection $billings
 * @property \Illuminate\Database\Eloquent\Collection $contracts
 * @property string $first_name
 * @property string $last_name
 * @property integer $dni
 * @property string $phone
 * @property string $birth
 * @property string $status
 * @property integer $user_id
 */
class Employee extends Model
{
    use SoftDeletes;

    public $table = 'employees';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'first_name',
        'last_name',
        'dni',
        'phone',
        'birth',
        'status',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'first_name' => 'string',
        'last_name' => 'string',
        'dni' => 'integer',
        'phone' => 'string',
        'birth' => 'date',
        'status' => 'string',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'first_name' => 'required',
        'last_name' => 'required',
        'dni' => 'required',
        'phone' => 'required',
        'birth' => 'required',
        'status' => 'required',
        'user_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\Profile\User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function billings()
    {
        return $this->hasMany(\App\Models\Profile\Billing::class, 'employee_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function contracts()
    {
        return $this->hasMany(\App\Models\Profile\Contract::class, 'employee_id');
    }
}

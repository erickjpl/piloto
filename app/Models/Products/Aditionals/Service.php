<?php

namespace App\Models\Products\Aditionals;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Service
 * @package App\Models\Products\Aditionals
 * @version May 16, 2020, 6:45 pm UTC
 *
 * @property string $service
 * @property string $slug
 * @property string $description
 * @property string $type_service
 * @property number $price
 */
class Service extends Model
{
    use SoftDeletes;

    public $table = 'services';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'service',
        'slug',
        'description',
        'type_service',
        'price'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'service' => 'string',
        'slug' => 'string',
        'description' => 'string',
        'type_service' => 'string',
        'price' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'service' => 'required',
        'slug' => 'required',
        'description' => 'required',
        'type_service' => 'required',
        'price' => 'required'
    ];

    
}

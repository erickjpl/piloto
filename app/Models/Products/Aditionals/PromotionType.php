<?php

namespace App\Models\Products\Aditionals;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PromotionType
 * @package App\Models\Products\Aditionals
 * @version May 16, 2020, 6:45 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $promotions
 * @property string $promotion
 * @property string $slug
 * @property string $description
 */
class PromotionType extends Model
{
    use SoftDeletes;

    public $table = 'promotion_types';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'promotion',
        'slug',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'promotion' => 'string',
        'slug' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'promotion' => 'required',
        'slug' => 'required',
        'description' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function promotions()
    {
        return $this->hasMany(\App\Models\Products\Aditionals\Promotion::class, 'promotion_type_id');
    }
}

<?php

namespace App\Models\Products\Aditionals;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Promotion
 * @package App\Models\Products\Aditionals
 * @version May 16, 2020, 6:45 pm UTC
 *
 * @property \App\Models\Products\Aditionals\PromotionType $promotionType
 * @property \Illuminate\Database\Eloquent\Collection $billingDetails
 * @property \Illuminate\Database\Eloquent\Collection $billings
 * @property string $promotion
 * @property string $slug
 * @property string $description
 * @property string $status
 * @property integer $promotion_type_id
 */
class Promotion extends Model
{
    use SoftDeletes;

    public $table = 'promotions';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'promotion',
        'slug',
        'description',
        'status',
        'promotion_type_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'promotion' => 'string',
        'slug' => 'string',
        'description' => 'string',
        'status' => 'string',
        'promotion_type_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'promotion' => 'required',
        'slug' => 'required',
        'description' => 'required',
        'status' => 'required',
        'promotion_type_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function promotionType()
    {
        return $this->belongsTo(\App\Models\Products\Aditionals\PromotionType::class, 'promotion_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function billingDetails()
    {
        return $this->hasMany(\App\Models\Products\Aditionals\BillingDetail::class, 'promotion_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function billings()
    {
        return $this->hasMany(\App\Models\Products\Aditionals\Billing::class, 'promotion_id');
    }
}

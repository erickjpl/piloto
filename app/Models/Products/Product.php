<?php

namespace App\Models\Products;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Product
 * @package App\Models\Products
 * @version May 16, 2020, 6:45 pm UTC
 *
 * @property \App\Models\Products\Brand $brand
 * @property \App\Models\Products\Category $category
 * @property \App\Models\Products\ProductType $productType
 * @property \App\Models\Products\Supplier $supplier
 * @property \Illuminate\Database\Eloquent\Collection $inventories
 * @property string $product
 * @property string $slug
 * @property string $description
 * @property integer $category_id
 * @property integer $brand_id
 * @property integer $product_type_id
 * @property integer $supplier_id
 */
class Product extends Model
{
    use SoftDeletes;

    public $table = 'products';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'product',
        'slug',
        'description',
        'category_id',
        'brand_id',
        'product_type_id',
        'supplier_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'product' => 'string',
        'slug' => 'string',
        'description' => 'string',
        'category_id' => 'integer',
        'brand_id' => 'integer',
        'product_type_id' => 'integer',
        'supplier_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'product' => 'required',
        'slug' => 'required',
        'description' => 'required',
        'category_id' => 'required',
        'brand_id' => 'required',
        'product_type_id' => 'required',
        'supplier_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function brand()
    {
        return $this->belongsTo(\App\Models\Products\Brand::class, 'brand_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function category()
    {
        return $this->belongsTo(\App\Models\Products\Category::class, 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function productType()
    {
        return $this->belongsTo(\App\Models\Products\ProductType::class, 'product_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function supplier()
    {
        return $this->belongsTo(\App\Models\Products\Supplier::class, 'supplier_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function inventories()
    {
        return $this->hasMany(\App\Models\Products\Inventory::class, 'product_id');
    }
}

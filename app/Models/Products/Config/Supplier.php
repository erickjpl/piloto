<?php

namespace App\Models\Products\Config;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Supplier
 * @package App\Models\Products\Config
 * @version May 16, 2020, 6:45 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $products
 * @property string $supplier
 * @property string $slug
 * @property string $email
 * @property string $address
 * @property string $phone
 * @property string $zip_code
 */
class Supplier extends Model
{
    use SoftDeletes;

    public $table = 'suppliers';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'supplier',
        'slug',
        'email',
        'address',
        'phone',
        'zip_code'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'supplier' => 'string',
        'slug' => 'string',
        'email' => 'string',
        'address' => 'string',
        'phone' => 'string',
        'zip_code' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'supplier' => 'required',
        'slug' => 'required',
        'email' => 'required',
        'address' => 'required',
        'phone' => 'required',
        'zip_code' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function products()
    {
        return $this->hasMany(\App\Models\Products\Config\Product::class, 'supplier_id');
    }
}

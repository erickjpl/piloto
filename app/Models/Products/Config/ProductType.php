<?php

namespace App\Models\Products\Config;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProductType
 * @package App\Models\Products\Config
 * @version May 16, 2020, 6:45 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $products
 * @property string $product_type
 * @property string $slug
 * @property string $description
 */
class ProductType extends Model
{
    use SoftDeletes;

    public $table = 'product_types';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'product_type',
        'slug',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'product_type' => 'string',
        'slug' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'product_type' => 'required',
        'slug' => 'required',
        'description' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function products()
    {
        return $this->hasMany(\App\Models\Products\Config\Product::class, 'product_type_id');
    }
}

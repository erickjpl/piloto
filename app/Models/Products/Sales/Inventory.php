<?php

namespace App\Models\Products\Sales;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Inventory
 * @package App\Models\Products\Sales
 * @version May 16, 2020, 6:45 pm UTC
 *
 * @property \App\Models\Products\Sales\Heritage $heritage
 * @property \App\Models\Products\Sales\Product $product
 * @property \Illuminate\Database\Eloquent\Collection $sales
 * @property string|\Carbon\Carbon $arrival
 * @property string|\Carbon\Carbon $due_date
 * @property string $product_presentation
 * @property integer $wholesale_quantity
 * @property number $purchase_price
 * @property string $status
 * @property string $observation
 * @property integer $product_id
 * @property integer $heritage_id
 */
class Inventory extends Model
{
    use SoftDeletes;

    public $table = 'inventories';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'arrival',
        'due_date',
        'product_presentation',
        'wholesale_quantity',
        'purchase_price',
        'status',
        'observation',
        'product_id',
        'heritage_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'arrival' => 'datetime',
        'due_date' => 'datetime',
        'product_presentation' => 'string',
        'wholesale_quantity' => 'integer',
        'purchase_price' => 'float',
        'status' => 'string',
        'observation' => 'string',
        'product_id' => 'integer',
        'heritage_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'arrival' => 'required',
        'due_date' => 'required',
        'product_presentation' => 'required',
        'wholesale_quantity' => 'required',
        'purchase_price' => 'required',
        'status' => 'required',
        'observation' => 'required',
        'product_id' => 'required',
        'heritage_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function heritage()
    {
        return $this->belongsTo(\App\Models\Products\Sales\Heritage::class, 'heritage_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function product()
    {
        return $this->belongsTo(\App\Models\Products\Sales\Product::class, 'product_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function sales()
    {
        return $this->hasMany(\App\Models\Products\Sales\Sale::class, 'inventory_id');
    }
}

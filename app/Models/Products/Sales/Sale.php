<?php

namespace App\Models\Products\Sales;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Sale
 * @package App\Models\Products\Sales
 * @version May 16, 2020, 6:45 pm UTC
 *
 * @property \App\Models\Products\Sales\Inventory $inventory
 * @property string $type_sale
 * @property string $condition
 * @property number $price
 * @property integer $inventory_id
 */
class Sale extends Model
{
    use SoftDeletes;

    public $table = 'sales';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'type_sale',
        'condition',
        'price',
        'inventory_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'type_sale' => 'string',
        'condition' => 'string',
        'price' => 'float',
        'inventory_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'type_sale' => 'required',
        'condition' => 'required',
        'price' => 'required',
        'inventory_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function inventory()
    {
        return $this->belongsTo(\App\Models\Products\Sales\Inventory::class, 'inventory_id');
    }
}

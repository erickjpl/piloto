<?php

namespace App\Models\Config\Suscribes;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Plan
 * @package App\Models\Config\Suscribes
 * @version May 16, 2020, 6:45 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $subscribes
 * @property string $plan
 * @property string $slug
 * @property string $description
 * @property string $time_frame
 * @property number $price
 * @property string $product_presentation
 */
class Plan extends Model
{
    use SoftDeletes;

    public $table = 'plans';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'plan',
        'slug',
        'description',
        'time_frame',
        'price',
        'product_presentation'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'plan' => 'string',
        'slug' => 'string',
        'description' => 'string',
        'time_frame' => 'string',
        'price' => 'float',
        'product_presentation' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'plan' => 'required',
        'slug' => 'required',
        'description' => 'required',
        'time_frame' => 'required',
        'price' => 'required',
        'product_presentation' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function subscribes()
    {
        return $this->hasMany(\App\Models\Config\Suscribes\Subscribe::class, 'plan_id');
    }
}

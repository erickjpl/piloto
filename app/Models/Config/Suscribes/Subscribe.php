<?php

namespace App\Models\Config\Suscribes;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Subscribe
 * @package App\Models\Config\Suscribes
 * @version May 16, 2020, 6:45 pm UTC
 *
 * @property \App\Models\Config\Suscribes\Plan $plan
 * @property \App\Models\Config\Suscribes\User $user
 * @property string|\Carbon\Carbon $start_date
 * @property string|\Carbon\Carbon $end_date
 * @property string $status
 * @property number $price
 * @property integer $user_id
 * @property integer $plan_id
 */
class Subscribe extends Model
{
    use SoftDeletes;

    public $table = 'subscribes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'start_date',
        'end_date',
        'status',
        'price',
        'user_id',
        'plan_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'start_date' => 'datetime',
        'end_date' => 'datetime',
        'status' => 'string',
        'price' => 'float',
        'user_id' => 'integer',
        'plan_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'start_date' => 'required',
        'end_date' => 'required',
        'status' => 'required',
        'price' => 'required',
        'user_id' => 'required',
        'plan_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function plan()
    {
        return $this->belongsTo(\App\Models\Config\Suscribes\Plan::class, 'plan_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\Config\Suscribes\User::class, 'user_id');
    }
}

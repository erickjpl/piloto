<?php

namespace App\Models\Config\Heritages;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Layout
 * @package App\Models\Config\Heritages
 * @version May 16, 2020, 6:44 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $heritages
 * @property string $layout
 * @property string $slug
 */
class Layout extends Model
{
    use SoftDeletes;

    public $table = 'layouts';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'layout',
        'slug'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'layout' => 'string',
        'slug' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'layout' => 'required',
        'slug' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function heritages()
    {
        return $this->hasMany(\App\Models\Config\Heritages\Heritage::class, 'layout_id');
    }
}

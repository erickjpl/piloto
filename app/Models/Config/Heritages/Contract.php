<?php

namespace App\Models\Config\Heritages;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Contract
 * @package App\Models\Config\Heritages
 * @version May 16, 2020, 6:45 pm UTC
 *
 * @property \App\Models\Config\Heritages\Employee $employee
 * @property string|\Carbon\Carbon $contract_date
 * @property number $salary
 * @property integer $employee_id
 */
class Contract extends Model
{
    use SoftDeletes;

    public $table = 'contracts';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'contract_date',
        'salary',
        'employee_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'contract_date' => 'datetime',
        'salary' => 'float',
        'employee_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'contract_date' => 'required',
        'salary' => 'required',
        'employee_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function employee()
    {
        return $this->belongsTo(\App\Models\Config\Heritages\Employee::class, 'employee_id');
    }
}

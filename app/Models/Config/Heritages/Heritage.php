<?php

namespace App\Models\Config\Heritages;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Heritage
 * @package App\Models\Config\Heritages
 * @version May 16, 2020, 6:45 pm UTC
 *
 * @property \App\Models\Config\Heritages\City $city
 * @property \App\Models\Config\Heritages\Layout $layout
 * @property \App\Models\Config\Heritages\User $user
 * @property \Illuminate\Database\Eloquent\Collection $inventories
 * @property string $name
 * @property string $slug
 * @property string $phone
 * @property string $address
 * @property string $email
 * @property string $zip_code
 * @property integer $headquarter
 * @property integer $user_id
 * @property integer $layout_id
 * @property integer $city_id
 */
class Heritage extends Model
{
    use SoftDeletes;

    public $table = 'heritages';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'slug',
        'phone',
        'address',
        'email',
        'zip_code',
        'headquarter',
        'user_id',
        'layout_id',
        'city_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'slug' => 'string',
        'phone' => 'string',
        'address' => 'string',
        'email' => 'string',
        'zip_code' => 'string',
        'headquarter' => 'integer',
        'user_id' => 'integer',
        'layout_id' => 'integer',
        'city_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'slug' => 'required',
        'phone' => 'required',
        'address' => 'required',
        'email' => 'required',
        'zip_code' => 'required',
        'headquarter' => 'required',
        'user_id' => 'required',
        'layout_id' => 'required',
        'city_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function city()
    {
        return $this->belongsTo(\App\Models\Config\Heritages\City::class, 'city_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function layout()
    {
        return $this->belongsTo(\App\Models\Config\Heritages\Layout::class, 'layout_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\Config\Heritages\User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function inventories()
    {
        return $this->hasMany(\App\Models\Config\Heritages\Inventory::class, 'heritage_id');
    }
}

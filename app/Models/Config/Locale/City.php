<?php

namespace App\Models\Config\Locale;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class City
 * @package App\Models\Config\Locale
 * @version May 16, 2020, 6:45 pm UTC
 *
 * @property \App\Models\Config\Locale\State $state
 * @property \Illuminate\Database\Eloquent\Collection $heritages
 * @property string $city
 * @property string $slug
 * @property integer $capital
 * @property integer $state_id
 */
class City extends Model
{
    use SoftDeletes;

    public $table = 'cities';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'city',
        'slug',
        'capital',
        'state_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'city' => 'string',
        'slug' => 'string',
        'capital' => 'integer',
        'state_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'city' => 'required',
        'slug' => 'required',
        'capital' => 'required',
        'state_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function state()
    {
        return $this->belongsTo(\App\Models\Config\Locale\State::class, 'state_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function heritages()
    {
        return $this->hasMany(\App\Models\Config\Locale\Heritage::class, 'city_id');
    }
}

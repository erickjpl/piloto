<?php

namespace App\Models\Config\Locale;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class State
 * @package App\Models\Config\Locale
 * @version May 16, 2020, 6:45 pm UTC
 *
 * @property \App\Models\Config\Locale\Country $country
 * @property \Illuminate\Database\Eloquent\Collection $cities
 * @property string $state
 * @property string $slug
 * @property integer $country_id
 */
class State extends Model
{
    use SoftDeletes;

    public $table = 'states';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'state',
        'slug',
        'country_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'state' => 'string',
        'slug' => 'string',
        'country_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'state' => 'required',
        'slug' => 'required',
        'country_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function country()
    {
        return $this->belongsTo(\App\Models\Config\Locale\Country::class, 'country_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function cities()
    {
        return $this->hasMany(\App\Models\Config\Locale\City::class, 'state_id');
    }
}

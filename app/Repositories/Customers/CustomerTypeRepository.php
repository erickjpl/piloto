<?php

namespace App\Repositories\Customers;

use App\Models\Customers\CustomerType;
use App\Repositories\BaseRepository;

/**
 * Class CustomerTypeRepository
 * @package App\Repositories\Customers
 * @version May 16, 2020, 6:45 pm UTC
*/

class CustomerTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'type',
        'slug',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CustomerType::class;
    }
}

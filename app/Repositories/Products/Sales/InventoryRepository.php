<?php

namespace App\Repositories\Products\Sales;

use App\Models\Products\Sales\Inventory;
use App\Repositories\BaseRepository;

/**
 * Class InventoryRepository
 * @package App\Repositories\Products\Sales
 * @version May 16, 2020, 6:45 pm UTC
*/

class InventoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'arrival',
        'due_date',
        'product_presentation',
        'wholesale_quantity',
        'purchase_price',
        'status',
        'observation',
        'product_id',
        'heritage_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Inventory::class;
    }
}

<?php

namespace App\Repositories\Products\Config;

use App\Models\Products\Config\ProductType;
use App\Repositories\BaseRepository;

/**
 * Class ProductTypeRepository
 * @package App\Repositories\Products\Config
 * @version May 16, 2020, 6:45 pm UTC
*/

class ProductTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_type',
        'slug',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProductType::class;
    }
}

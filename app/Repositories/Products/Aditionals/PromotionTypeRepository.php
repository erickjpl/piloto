<?php

namespace App\Repositories\Products\Aditionals;

use App\Models\Products\Aditionals\PromotionType;
use App\Repositories\BaseRepository;

/**
 * Class PromotionTypeRepository
 * @package App\Repositories\Products\Aditionals
 * @version May 16, 2020, 6:45 pm UTC
*/

class PromotionTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'promotion',
        'slug',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PromotionType::class;
    }
}

<?php

namespace App\Repositories\Config\Locale;

use App\Models\Config\Locale\State;
use App\Repositories\BaseRepository;

/**
 * Class StateRepository
 * @package App\Repositories\Config\Locale
 * @version May 16, 2020, 6:45 pm UTC
*/

class StateRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'state',
        'slug',
        'country_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return State::class;
    }
}

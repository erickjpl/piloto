<?php

namespace App\Repositories\Config\Locale;

use App\Models\Config\Locale\City;
use App\Repositories\BaseRepository;

/**
 * Class CityRepository
 * @package App\Repositories\Config\Locale
 * @version May 16, 2020, 6:45 pm UTC
*/

class CityRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'city',
        'slug',
        'capital',
        'state_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return City::class;
    }
}

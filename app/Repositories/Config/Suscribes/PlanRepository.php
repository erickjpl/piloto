<?php

namespace App\Repositories\Config\Suscribes;

use App\Models\Config\Suscribes\Plan;
use App\Repositories\BaseRepository;

/**
 * Class PlanRepository
 * @package App\Repositories\Config\Suscribes
 * @version May 16, 2020, 6:45 pm UTC
*/

class PlanRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'plan',
        'slug',
        'description',
        'time_frame',
        'price',
        'product_presentation'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Plan::class;
    }
}

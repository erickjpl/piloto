<?php

namespace App\Repositories\Config\Suscribes;

use App\Models\Config\Suscribes\Subscribe;
use App\Repositories\BaseRepository;

/**
 * Class SubscribeRepository
 * @package App\Repositories\Config\Suscribes
 * @version May 16, 2020, 6:45 pm UTC
*/

class SubscribeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'start_date',
        'end_date',
        'status',
        'price',
        'user_id',
        'plan_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Subscribe::class;
    }
}

<?php

namespace App\Repositories\Config\Heritages;

use App\Models\Config\Heritages\Layout;
use App\Repositories\BaseRepository;

/**
 * Class LayoutRepository
 * @package App\Repositories\Config\Heritages
 * @version May 16, 2020, 6:44 pm UTC
*/

class LayoutRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'layout',
        'slug'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Layout::class;
    }
}

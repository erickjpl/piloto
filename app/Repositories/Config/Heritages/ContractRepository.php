<?php

namespace App\Repositories\Config\Heritages;

use App\Models\Config\Heritages\Contract;
use App\Repositories\BaseRepository;

/**
 * Class ContractRepository
 * @package App\Repositories\Config\Heritages
 * @version May 16, 2020, 6:45 pm UTC
*/

class ContractRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'contract_date',
        'salary',
        'employee_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Contract::class;
    }
}

<?php

namespace App\Repositories\Config\Heritages;

use App\Models\Config\Heritages\Heritage;
use App\Repositories\BaseRepository;

/**
 * Class HeritageRepository
 * @package App\Repositories\Config\Heritages
 * @version May 16, 2020, 6:45 pm UTC
*/

class HeritageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'slug',
        'phone',
        'address',
        'email',
        'zip_code',
        'headquarter',
        'user_id',
        'layout_id',
        'city_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Heritage::class;
    }
}

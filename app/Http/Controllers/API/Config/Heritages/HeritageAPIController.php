<?php

namespace App\Http\Controllers\API\Config\Heritages;

use App\Http\Requests\API\Config\Heritages\CreateHeritageAPIRequest;
use App\Http\Requests\API\Config\Heritages\UpdateHeritageAPIRequest;
use App\Models\Config\Heritages\Heritage;
use App\Repositories\Config\Heritages\HeritageRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class HeritageController
 * @package App\Http\Controllers\API\Config\Heritages
 */

class HeritageAPIController extends AppBaseController
{
    /** @var  HeritageRepository */
    private $heritageRepository;

    public function __construct(HeritageRepository $heritageRepo)
    {
        $this->heritageRepository = $heritageRepo;
    }

    /**
     * Display a listing of the Heritage.
     * GET|HEAD /heritages
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $heritages = $this->heritageRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($heritages->toArray(), 'Heritages retrieved successfully');
    }

    /**
     * Store a newly created Heritage in storage.
     * POST /heritages
     *
     * @param CreateHeritageAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateHeritageAPIRequest $request)
    {
        $input = $request->all();

        $heritage = $this->heritageRepository->create($input);

        return $this->sendResponse($heritage->toArray(), 'Heritage saved successfully');
    }

    /**
     * Display the specified Heritage.
     * GET|HEAD /heritages/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Heritage $heritage */
        $heritage = $this->heritageRepository->find($id);

        if (empty($heritage)) {
            return $this->sendError('Heritage not found');
        }

        return $this->sendResponse($heritage->toArray(), 'Heritage retrieved successfully');
    }

    /**
     * Update the specified Heritage in storage.
     * PUT/PATCH /heritages/{id}
     *
     * @param int $id
     * @param UpdateHeritageAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHeritageAPIRequest $request)
    {
        $input = $request->all();

        /** @var Heritage $heritage */
        $heritage = $this->heritageRepository->find($id);

        if (empty($heritage)) {
            return $this->sendError('Heritage not found');
        }

        $heritage = $this->heritageRepository->update($input, $id);

        return $this->sendResponse($heritage->toArray(), 'Heritage updated successfully');
    }

    /**
     * Remove the specified Heritage from storage.
     * DELETE /heritages/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Heritage $heritage */
        $heritage = $this->heritageRepository->find($id);

        if (empty($heritage)) {
            return $this->sendError('Heritage not found');
        }

        $heritage->delete();

        return $this->sendSuccess('Heritage deleted successfully');
    }
}

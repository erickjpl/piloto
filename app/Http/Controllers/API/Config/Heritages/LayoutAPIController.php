<?php

namespace App\Http\Controllers\API\Config\Heritages;

use App\Http\Requests\API\Config\Heritages\CreateLayoutAPIRequest;
use App\Http\Requests\API\Config\Heritages\UpdateLayoutAPIRequest;
use App\Models\Config\Heritages\Layout;
use App\Repositories\Config\Heritages\LayoutRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class LayoutController
 * @package App\Http\Controllers\API\Config\Heritages
 */

class LayoutAPIController extends AppBaseController
{
    /** @var  LayoutRepository */
    private $layoutRepository;

    public function __construct(LayoutRepository $layoutRepo)
    {
        $this->layoutRepository = $layoutRepo;
    }

    /**
     * Display a listing of the Layout.
     * GET|HEAD /layouts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $layouts = $this->layoutRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($layouts->toArray(), 'Layouts retrieved successfully');
    }

    /**
     * Store a newly created Layout in storage.
     * POST /layouts
     *
     * @param CreateLayoutAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateLayoutAPIRequest $request)
    {
        $input = $request->all();

        $layout = $this->layoutRepository->create($input);

        return $this->sendResponse($layout->toArray(), 'Layout saved successfully');
    }

    /**
     * Display the specified Layout.
     * GET|HEAD /layouts/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Layout $layout */
        $layout = $this->layoutRepository->find($id);

        if (empty($layout)) {
            return $this->sendError('Layout not found');
        }

        return $this->sendResponse($layout->toArray(), 'Layout retrieved successfully');
    }

    /**
     * Update the specified Layout in storage.
     * PUT/PATCH /layouts/{id}
     *
     * @param int $id
     * @param UpdateLayoutAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLayoutAPIRequest $request)
    {
        $input = $request->all();

        /** @var Layout $layout */
        $layout = $this->layoutRepository->find($id);

        if (empty($layout)) {
            return $this->sendError('Layout not found');
        }

        $layout = $this->layoutRepository->update($input, $id);

        return $this->sendResponse($layout->toArray(), 'Layout updated successfully');
    }

    /**
     * Remove the specified Layout from storage.
     * DELETE /layouts/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Layout $layout */
        $layout = $this->layoutRepository->find($id);

        if (empty($layout)) {
            return $this->sendError('Layout not found');
        }

        $layout->delete();

        return $this->sendSuccess('Layout deleted successfully');
    }
}

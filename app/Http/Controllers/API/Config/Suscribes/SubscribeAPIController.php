<?php

namespace App\Http\Controllers\API\Config\Suscribes;

use App\Http\Requests\API\Config\Suscribes\CreateSubscribeAPIRequest;
use App\Http\Requests\API\Config\Suscribes\UpdateSubscribeAPIRequest;
use App\Models\Config\Suscribes\Subscribe;
use App\Repositories\Config\Suscribes\SubscribeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class SubscribeController
 * @package App\Http\Controllers\API\Config\Suscribes
 */

class SubscribeAPIController extends AppBaseController
{
    /** @var  SubscribeRepository */
    private $subscribeRepository;

    public function __construct(SubscribeRepository $subscribeRepo)
    {
        $this->subscribeRepository = $subscribeRepo;
    }

    /**
     * Display a listing of the Subscribe.
     * GET|HEAD /subscribes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $subscribes = $this->subscribeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($subscribes->toArray(), 'Subscribes retrieved successfully');
    }

    /**
     * Store a newly created Subscribe in storage.
     * POST /subscribes
     *
     * @param CreateSubscribeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSubscribeAPIRequest $request)
    {
        $input = $request->all();

        $subscribe = $this->subscribeRepository->create($input);

        return $this->sendResponse($subscribe->toArray(), 'Subscribe saved successfully');
    }

    /**
     * Display the specified Subscribe.
     * GET|HEAD /subscribes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Subscribe $subscribe */
        $subscribe = $this->subscribeRepository->find($id);

        if (empty($subscribe)) {
            return $this->sendError('Subscribe not found');
        }

        return $this->sendResponse($subscribe->toArray(), 'Subscribe retrieved successfully');
    }

    /**
     * Update the specified Subscribe in storage.
     * PUT/PATCH /subscribes/{id}
     *
     * @param int $id
     * @param UpdateSubscribeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSubscribeAPIRequest $request)
    {
        $input = $request->all();

        /** @var Subscribe $subscribe */
        $subscribe = $this->subscribeRepository->find($id);

        if (empty($subscribe)) {
            return $this->sendError('Subscribe not found');
        }

        $subscribe = $this->subscribeRepository->update($input, $id);

        return $this->sendResponse($subscribe->toArray(), 'Subscribe updated successfully');
    }

    /**
     * Remove the specified Subscribe from storage.
     * DELETE /subscribes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Subscribe $subscribe */
        $subscribe = $this->subscribeRepository->find($id);

        if (empty($subscribe)) {
            return $this->sendError('Subscribe not found');
        }

        $subscribe->delete();

        return $this->sendSuccess('Subscribe deleted successfully');
    }
}

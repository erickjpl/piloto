<?php

namespace App\Http\Controllers\API\Products\Aditionals;

use App\Http\Requests\API\Products\Aditionals\CreatePromotionTypeAPIRequest;
use App\Http\Requests\API\Products\Aditionals\UpdatePromotionTypeAPIRequest;
use App\Models\Products\Aditionals\PromotionType;
use App\Repositories\Products\Aditionals\PromotionTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PromotionTypeController
 * @package App\Http\Controllers\API\Products\Aditionals
 */

class PromotionTypeAPIController extends AppBaseController
{
    /** @var  PromotionTypeRepository */
    private $promotionTypeRepository;

    public function __construct(PromotionTypeRepository $promotionTypeRepo)
    {
        $this->promotionTypeRepository = $promotionTypeRepo;
    }

    /**
     * Display a listing of the PromotionType.
     * GET|HEAD /promotionTypes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $promotionTypes = $this->promotionTypeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($promotionTypes->toArray(), 'Promotion Types retrieved successfully');
    }

    /**
     * Store a newly created PromotionType in storage.
     * POST /promotionTypes
     *
     * @param CreatePromotionTypeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePromotionTypeAPIRequest $request)
    {
        $input = $request->all();

        $promotionType = $this->promotionTypeRepository->create($input);

        return $this->sendResponse($promotionType->toArray(), 'Promotion Type saved successfully');
    }

    /**
     * Display the specified PromotionType.
     * GET|HEAD /promotionTypes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PromotionType $promotionType */
        $promotionType = $this->promotionTypeRepository->find($id);

        if (empty($promotionType)) {
            return $this->sendError('Promotion Type not found');
        }

        return $this->sendResponse($promotionType->toArray(), 'Promotion Type retrieved successfully');
    }

    /**
     * Update the specified PromotionType in storage.
     * PUT/PATCH /promotionTypes/{id}
     *
     * @param int $id
     * @param UpdatePromotionTypeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePromotionTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var PromotionType $promotionType */
        $promotionType = $this->promotionTypeRepository->find($id);

        if (empty($promotionType)) {
            return $this->sendError('Promotion Type not found');
        }

        $promotionType = $this->promotionTypeRepository->update($input, $id);

        return $this->sendResponse($promotionType->toArray(), 'PromotionType updated successfully');
    }

    /**
     * Remove the specified PromotionType from storage.
     * DELETE /promotionTypes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PromotionType $promotionType */
        $promotionType = $this->promotionTypeRepository->find($id);

        if (empty($promotionType)) {
            return $this->sendError('Promotion Type not found');
        }

        $promotionType->delete();

        return $this->sendSuccess('Promotion Type deleted successfully');
    }
}

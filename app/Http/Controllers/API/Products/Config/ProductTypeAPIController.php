<?php

namespace App\Http\Controllers\API\Products\Config;

use App\Http\Requests\API\Products\Config\CreateProductTypeAPIRequest;
use App\Http\Requests\API\Products\Config\UpdateProductTypeAPIRequest;
use App\Models\Products\Config\ProductType;
use App\Repositories\Products\Config\ProductTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ProductTypeController
 * @package App\Http\Controllers\API\Products\Config
 */

class ProductTypeAPIController extends AppBaseController
{
    /** @var  ProductTypeRepository */
    private $productTypeRepository;

    public function __construct(ProductTypeRepository $productTypeRepo)
    {
        $this->productTypeRepository = $productTypeRepo;
    }

    /**
     * Display a listing of the ProductType.
     * GET|HEAD /productTypes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $productTypes = $this->productTypeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($productTypes->toArray(), 'Product Types retrieved successfully');
    }

    /**
     * Store a newly created ProductType in storage.
     * POST /productTypes
     *
     * @param CreateProductTypeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateProductTypeAPIRequest $request)
    {
        $input = $request->all();

        $productType = $this->productTypeRepository->create($input);

        return $this->sendResponse($productType->toArray(), 'Product Type saved successfully');
    }

    /**
     * Display the specified ProductType.
     * GET|HEAD /productTypes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ProductType $productType */
        $productType = $this->productTypeRepository->find($id);

        if (empty($productType)) {
            return $this->sendError('Product Type not found');
        }

        return $this->sendResponse($productType->toArray(), 'Product Type retrieved successfully');
    }

    /**
     * Update the specified ProductType in storage.
     * PUT/PATCH /productTypes/{id}
     *
     * @param int $id
     * @param UpdateProductTypeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var ProductType $productType */
        $productType = $this->productTypeRepository->find($id);

        if (empty($productType)) {
            return $this->sendError('Product Type not found');
        }

        $productType = $this->productTypeRepository->update($input, $id);

        return $this->sendResponse($productType->toArray(), 'ProductType updated successfully');
    }

    /**
     * Remove the specified ProductType from storage.
     * DELETE /productTypes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ProductType $productType */
        $productType = $this->productTypeRepository->find($id);

        if (empty($productType)) {
            return $this->sendError('Product Type not found');
        }

        $productType->delete();

        return $this->sendSuccess('Product Type deleted successfully');
    }
}

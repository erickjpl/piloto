<?php

namespace App\Http\Controllers\API\Customers;

use App\Http\Requests\API\Customers\CreateCustomerTypeAPIRequest;
use App\Http\Requests\API\Customers\UpdateCustomerTypeAPIRequest;
use App\Models\Customers\CustomerType;
use App\Repositories\Customers\CustomerTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class CustomerTypeController
 * @package App\Http\Controllers\API\Customers
 */

class CustomerTypeAPIController extends AppBaseController
{
    /** @var  CustomerTypeRepository */
    private $customerTypeRepository;

    public function __construct(CustomerTypeRepository $customerTypeRepo)
    {
        $this->customerTypeRepository = $customerTypeRepo;
    }

    /**
     * Display a listing of the CustomerType.
     * GET|HEAD /customerTypes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $customerTypes = $this->customerTypeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($customerTypes->toArray(), 'Customer Types retrieved successfully');
    }

    /**
     * Store a newly created CustomerType in storage.
     * POST /customerTypes
     *
     * @param CreateCustomerTypeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerTypeAPIRequest $request)
    {
        $input = $request->all();

        $customerType = $this->customerTypeRepository->create($input);

        return $this->sendResponse($customerType->toArray(), 'Customer Type saved successfully');
    }

    /**
     * Display the specified CustomerType.
     * GET|HEAD /customerTypes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CustomerType $customerType */
        $customerType = $this->customerTypeRepository->find($id);

        if (empty($customerType)) {
            return $this->sendError('Customer Type not found');
        }

        return $this->sendResponse($customerType->toArray(), 'Customer Type retrieved successfully');
    }

    /**
     * Update the specified CustomerType in storage.
     * PUT/PATCH /customerTypes/{id}
     *
     * @param int $id
     * @param UpdateCustomerTypeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomerTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var CustomerType $customerType */
        $customerType = $this->customerTypeRepository->find($id);

        if (empty($customerType)) {
            return $this->sendError('Customer Type not found');
        }

        $customerType = $this->customerTypeRepository->update($input, $id);

        return $this->sendResponse($customerType->toArray(), 'CustomerType updated successfully');
    }

    /**
     * Remove the specified CustomerType from storage.
     * DELETE /customerTypes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CustomerType $customerType */
        $customerType = $this->customerTypeRepository->find($id);

        if (empty($customerType)) {
            return $this->sendError('Customer Type not found');
        }

        $customerType->delete();

        return $this->sendSuccess('Customer Type deleted successfully');
    }
}

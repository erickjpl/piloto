<?php

namespace App\Http\Controllers\API\Billings;

use App\Http\Requests\API\Billings\CreateBillingDetailAPIRequest;
use App\Http\Requests\API\Billings\UpdateBillingDetailAPIRequest;
use App\Models\Billings\BillingDetail;
use App\Repositories\Billings\BillingDetailRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class BillingDetailController
 * @package App\Http\Controllers\API\Billings
 */

class BillingDetailAPIController extends AppBaseController
{
    /** @var  BillingDetailRepository */
    private $billingDetailRepository;

    public function __construct(BillingDetailRepository $billingDetailRepo)
    {
        $this->billingDetailRepository = $billingDetailRepo;
    }

    /**
     * Display a listing of the BillingDetail.
     * GET|HEAD /billingDetails
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $billingDetails = $this->billingDetailRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($billingDetails->toArray(), 'Billing Details retrieved successfully');
    }

    /**
     * Store a newly created BillingDetail in storage.
     * POST /billingDetails
     *
     * @param CreateBillingDetailAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBillingDetailAPIRequest $request)
    {
        $input = $request->all();

        $billingDetail = $this->billingDetailRepository->create($input);

        return $this->sendResponse($billingDetail->toArray(), 'Billing Detail saved successfully');
    }

    /**
     * Display the specified BillingDetail.
     * GET|HEAD /billingDetails/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var BillingDetail $billingDetail */
        $billingDetail = $this->billingDetailRepository->find($id);

        if (empty($billingDetail)) {
            return $this->sendError('Billing Detail not found');
        }

        return $this->sendResponse($billingDetail->toArray(), 'Billing Detail retrieved successfully');
    }

    /**
     * Update the specified BillingDetail in storage.
     * PUT/PATCH /billingDetails/{id}
     *
     * @param int $id
     * @param UpdateBillingDetailAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBillingDetailAPIRequest $request)
    {
        $input = $request->all();

        /** @var BillingDetail $billingDetail */
        $billingDetail = $this->billingDetailRepository->find($id);

        if (empty($billingDetail)) {
            return $this->sendError('Billing Detail not found');
        }

        $billingDetail = $this->billingDetailRepository->update($input, $id);

        return $this->sendResponse($billingDetail->toArray(), 'BillingDetail updated successfully');
    }

    /**
     * Remove the specified BillingDetail from storage.
     * DELETE /billingDetails/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var BillingDetail $billingDetail */
        $billingDetail = $this->billingDetailRepository->find($id);

        if (empty($billingDetail)) {
            return $this->sendError('Billing Detail not found');
        }

        $billingDetail->delete();

        return $this->sendSuccess('Billing Detail deleted successfully');
    }
}

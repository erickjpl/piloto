<?php

namespace App\Http\Requests\API\Config\Heritages;

use App\Models\Config\Heritages\Layout;
use InfyOm\Generator\Request\APIRequest;

class UpdateLayoutAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Layout::$rules;
        
        return $rules;
    }
}

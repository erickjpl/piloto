<?php

namespace App\Http\Requests\API\Config\Suscribes;

use App\Models\Config\Suscribes\Subscribe;
use InfyOm\Generator\Request\APIRequest;

class CreateSubscribeAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Subscribe::$rules;
    }
}

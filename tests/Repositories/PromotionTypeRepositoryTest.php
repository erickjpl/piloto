<?php namespace Tests\Repositories;

use App\Models\Products\Aditionals\PromotionType;
use App\Repositories\Products\Aditionals\PromotionTypeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class PromotionTypeRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var PromotionTypeRepository
     */
    protected $promotionTypeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->promotionTypeRepo = \App::make(PromotionTypeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_promotion_type()
    {
        $promotionType = factory(PromotionType::class)->make()->toArray();

        $createdPromotionType = $this->promotionTypeRepo->create($promotionType);

        $createdPromotionType = $createdPromotionType->toArray();
        $this->assertArrayHasKey('id', $createdPromotionType);
        $this->assertNotNull($createdPromotionType['id'], 'Created PromotionType must have id specified');
        $this->assertNotNull(PromotionType::find($createdPromotionType['id']), 'PromotionType with given id must be in DB');
        $this->assertModelData($promotionType, $createdPromotionType);
    }

    /**
     * @test read
     */
    public function test_read_promotion_type()
    {
        $promotionType = factory(PromotionType::class)->create();

        $dbPromotionType = $this->promotionTypeRepo->find($promotionType->id);

        $dbPromotionType = $dbPromotionType->toArray();
        $this->assertModelData($promotionType->toArray(), $dbPromotionType);
    }

    /**
     * @test update
     */
    public function test_update_promotion_type()
    {
        $promotionType = factory(PromotionType::class)->create();
        $fakePromotionType = factory(PromotionType::class)->make()->toArray();

        $updatedPromotionType = $this->promotionTypeRepo->update($fakePromotionType, $promotionType->id);

        $this->assertModelData($fakePromotionType, $updatedPromotionType->toArray());
        $dbPromotionType = $this->promotionTypeRepo->find($promotionType->id);
        $this->assertModelData($fakePromotionType, $dbPromotionType->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_promotion_type()
    {
        $promotionType = factory(PromotionType::class)->create();

        $resp = $this->promotionTypeRepo->delete($promotionType->id);

        $this->assertTrue($resp);
        $this->assertNull(PromotionType::find($promotionType->id), 'PromotionType should not exist in DB');
    }
}

<?php namespace Tests\Repositories;

use App\Models\Config\Heritages\Layout;
use App\Repositories\Config\Heritages\LayoutRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class LayoutRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var LayoutRepository
     */
    protected $layoutRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->layoutRepo = \App::make(LayoutRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_layout()
    {
        $layout = factory(Layout::class)->make()->toArray();

        $createdLayout = $this->layoutRepo->create($layout);

        $createdLayout = $createdLayout->toArray();
        $this->assertArrayHasKey('id', $createdLayout);
        $this->assertNotNull($createdLayout['id'], 'Created Layout must have id specified');
        $this->assertNotNull(Layout::find($createdLayout['id']), 'Layout with given id must be in DB');
        $this->assertModelData($layout, $createdLayout);
    }

    /**
     * @test read
     */
    public function test_read_layout()
    {
        $layout = factory(Layout::class)->create();

        $dbLayout = $this->layoutRepo->find($layout->id);

        $dbLayout = $dbLayout->toArray();
        $this->assertModelData($layout->toArray(), $dbLayout);
    }

    /**
     * @test update
     */
    public function test_update_layout()
    {
        $layout = factory(Layout::class)->create();
        $fakeLayout = factory(Layout::class)->make()->toArray();

        $updatedLayout = $this->layoutRepo->update($fakeLayout, $layout->id);

        $this->assertModelData($fakeLayout, $updatedLayout->toArray());
        $dbLayout = $this->layoutRepo->find($layout->id);
        $this->assertModelData($fakeLayout, $dbLayout->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_layout()
    {
        $layout = factory(Layout::class)->create();

        $resp = $this->layoutRepo->delete($layout->id);

        $this->assertTrue($resp);
        $this->assertNull(Layout::find($layout->id), 'Layout should not exist in DB');
    }
}

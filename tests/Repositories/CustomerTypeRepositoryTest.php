<?php namespace Tests\Repositories;

use App\Models\Customers\CustomerType;
use App\Repositories\Customers\CustomerTypeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class CustomerTypeRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var CustomerTypeRepository
     */
    protected $customerTypeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->customerTypeRepo = \App::make(CustomerTypeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_customer_type()
    {
        $customerType = factory(CustomerType::class)->make()->toArray();

        $createdCustomerType = $this->customerTypeRepo->create($customerType);

        $createdCustomerType = $createdCustomerType->toArray();
        $this->assertArrayHasKey('id', $createdCustomerType);
        $this->assertNotNull($createdCustomerType['id'], 'Created CustomerType must have id specified');
        $this->assertNotNull(CustomerType::find($createdCustomerType['id']), 'CustomerType with given id must be in DB');
        $this->assertModelData($customerType, $createdCustomerType);
    }

    /**
     * @test read
     */
    public function test_read_customer_type()
    {
        $customerType = factory(CustomerType::class)->create();

        $dbCustomerType = $this->customerTypeRepo->find($customerType->id);

        $dbCustomerType = $dbCustomerType->toArray();
        $this->assertModelData($customerType->toArray(), $dbCustomerType);
    }

    /**
     * @test update
     */
    public function test_update_customer_type()
    {
        $customerType = factory(CustomerType::class)->create();
        $fakeCustomerType = factory(CustomerType::class)->make()->toArray();

        $updatedCustomerType = $this->customerTypeRepo->update($fakeCustomerType, $customerType->id);

        $this->assertModelData($fakeCustomerType, $updatedCustomerType->toArray());
        $dbCustomerType = $this->customerTypeRepo->find($customerType->id);
        $this->assertModelData($fakeCustomerType, $dbCustomerType->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_customer_type()
    {
        $customerType = factory(CustomerType::class)->create();

        $resp = $this->customerTypeRepo->delete($customerType->id);

        $this->assertTrue($resp);
        $this->assertNull(CustomerType::find($customerType->id), 'CustomerType should not exist in DB');
    }
}

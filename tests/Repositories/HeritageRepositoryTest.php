<?php namespace Tests\Repositories;

use App\Models\Config\Heritages\Heritage;
use App\Repositories\Config\Heritages\HeritageRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class HeritageRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var HeritageRepository
     */
    protected $heritageRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->heritageRepo = \App::make(HeritageRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_heritage()
    {
        $heritage = factory(Heritage::class)->make()->toArray();

        $createdHeritage = $this->heritageRepo->create($heritage);

        $createdHeritage = $createdHeritage->toArray();
        $this->assertArrayHasKey('id', $createdHeritage);
        $this->assertNotNull($createdHeritage['id'], 'Created Heritage must have id specified');
        $this->assertNotNull(Heritage::find($createdHeritage['id']), 'Heritage with given id must be in DB');
        $this->assertModelData($heritage, $createdHeritage);
    }

    /**
     * @test read
     */
    public function test_read_heritage()
    {
        $heritage = factory(Heritage::class)->create();

        $dbHeritage = $this->heritageRepo->find($heritage->id);

        $dbHeritage = $dbHeritage->toArray();
        $this->assertModelData($heritage->toArray(), $dbHeritage);
    }

    /**
     * @test update
     */
    public function test_update_heritage()
    {
        $heritage = factory(Heritage::class)->create();
        $fakeHeritage = factory(Heritage::class)->make()->toArray();

        $updatedHeritage = $this->heritageRepo->update($fakeHeritage, $heritage->id);

        $this->assertModelData($fakeHeritage, $updatedHeritage->toArray());
        $dbHeritage = $this->heritageRepo->find($heritage->id);
        $this->assertModelData($fakeHeritage, $dbHeritage->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_heritage()
    {
        $heritage = factory(Heritage::class)->create();

        $resp = $this->heritageRepo->delete($heritage->id);

        $this->assertTrue($resp);
        $this->assertNull(Heritage::find($heritage->id), 'Heritage should not exist in DB');
    }
}

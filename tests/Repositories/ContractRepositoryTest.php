<?php namespace Tests\Repositories;

use App\Models\Config\Heritages\Contract;
use App\Repositories\Config\Heritages\ContractRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ContractRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ContractRepository
     */
    protected $contractRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->contractRepo = \App::make(ContractRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_contract()
    {
        $contract = factory(Contract::class)->make()->toArray();

        $createdContract = $this->contractRepo->create($contract);

        $createdContract = $createdContract->toArray();
        $this->assertArrayHasKey('id', $createdContract);
        $this->assertNotNull($createdContract['id'], 'Created Contract must have id specified');
        $this->assertNotNull(Contract::find($createdContract['id']), 'Contract with given id must be in DB');
        $this->assertModelData($contract, $createdContract);
    }

    /**
     * @test read
     */
    public function test_read_contract()
    {
        $contract = factory(Contract::class)->create();

        $dbContract = $this->contractRepo->find($contract->id);

        $dbContract = $dbContract->toArray();
        $this->assertModelData($contract->toArray(), $dbContract);
    }

    /**
     * @test update
     */
    public function test_update_contract()
    {
        $contract = factory(Contract::class)->create();
        $fakeContract = factory(Contract::class)->make()->toArray();

        $updatedContract = $this->contractRepo->update($fakeContract, $contract->id);

        $this->assertModelData($fakeContract, $updatedContract->toArray());
        $dbContract = $this->contractRepo->find($contract->id);
        $this->assertModelData($fakeContract, $dbContract->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_contract()
    {
        $contract = factory(Contract::class)->create();

        $resp = $this->contractRepo->delete($contract->id);

        $this->assertTrue($resp);
        $this->assertNull(Contract::find($contract->id), 'Contract should not exist in DB');
    }
}

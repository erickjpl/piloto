<?php namespace Tests\Repositories;

use App\Models\Config\Suscribes\Subscribe;
use App\Repositories\Config\Suscribes\SubscribeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SubscribeRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SubscribeRepository
     */
    protected $subscribeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->subscribeRepo = \App::make(SubscribeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_subscribe()
    {
        $subscribe = factory(Subscribe::class)->make()->toArray();

        $createdSubscribe = $this->subscribeRepo->create($subscribe);

        $createdSubscribe = $createdSubscribe->toArray();
        $this->assertArrayHasKey('id', $createdSubscribe);
        $this->assertNotNull($createdSubscribe['id'], 'Created Subscribe must have id specified');
        $this->assertNotNull(Subscribe::find($createdSubscribe['id']), 'Subscribe with given id must be in DB');
        $this->assertModelData($subscribe, $createdSubscribe);
    }

    /**
     * @test read
     */
    public function test_read_subscribe()
    {
        $subscribe = factory(Subscribe::class)->create();

        $dbSubscribe = $this->subscribeRepo->find($subscribe->id);

        $dbSubscribe = $dbSubscribe->toArray();
        $this->assertModelData($subscribe->toArray(), $dbSubscribe);
    }

    /**
     * @test update
     */
    public function test_update_subscribe()
    {
        $subscribe = factory(Subscribe::class)->create();
        $fakeSubscribe = factory(Subscribe::class)->make()->toArray();

        $updatedSubscribe = $this->subscribeRepo->update($fakeSubscribe, $subscribe->id);

        $this->assertModelData($fakeSubscribe, $updatedSubscribe->toArray());
        $dbSubscribe = $this->subscribeRepo->find($subscribe->id);
        $this->assertModelData($fakeSubscribe, $dbSubscribe->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_subscribe()
    {
        $subscribe = factory(Subscribe::class)->create();

        $resp = $this->subscribeRepo->delete($subscribe->id);

        $this->assertTrue($resp);
        $this->assertNull(Subscribe::find($subscribe->id), 'Subscribe should not exist in DB');
    }
}

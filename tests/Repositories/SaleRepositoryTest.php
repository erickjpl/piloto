<?php namespace Tests\Repositories;

use App\Models\Products\Sales\Sale;
use App\Repositories\Products\Sales\SaleRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SaleRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SaleRepository
     */
    protected $saleRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->saleRepo = \App::make(SaleRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_sale()
    {
        $sale = factory(Sale::class)->make()->toArray();

        $createdSale = $this->saleRepo->create($sale);

        $createdSale = $createdSale->toArray();
        $this->assertArrayHasKey('id', $createdSale);
        $this->assertNotNull($createdSale['id'], 'Created Sale must have id specified');
        $this->assertNotNull(Sale::find($createdSale['id']), 'Sale with given id must be in DB');
        $this->assertModelData($sale, $createdSale);
    }

    /**
     * @test read
     */
    public function test_read_sale()
    {
        $sale = factory(Sale::class)->create();

        $dbSale = $this->saleRepo->find($sale->id);

        $dbSale = $dbSale->toArray();
        $this->assertModelData($sale->toArray(), $dbSale);
    }

    /**
     * @test update
     */
    public function test_update_sale()
    {
        $sale = factory(Sale::class)->create();
        $fakeSale = factory(Sale::class)->make()->toArray();

        $updatedSale = $this->saleRepo->update($fakeSale, $sale->id);

        $this->assertModelData($fakeSale, $updatedSale->toArray());
        $dbSale = $this->saleRepo->find($sale->id);
        $this->assertModelData($fakeSale, $dbSale->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_sale()
    {
        $sale = factory(Sale::class)->create();

        $resp = $this->saleRepo->delete($sale->id);

        $this->assertTrue($resp);
        $this->assertNull(Sale::find($sale->id), 'Sale should not exist in DB');
    }
}

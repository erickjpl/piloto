<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Products\Sales\Sale;

class SaleApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_sale()
    {
        $sale = factory(Sale::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/products/sales/sales', $sale
        );

        $this->assertApiResponse($sale);
    }

    /**
     * @test
     */
    public function test_read_sale()
    {
        $sale = factory(Sale::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/products/sales/sales/'.$sale->id
        );

        $this->assertApiResponse($sale->toArray());
    }

    /**
     * @test
     */
    public function test_update_sale()
    {
        $sale = factory(Sale::class)->create();
        $editedSale = factory(Sale::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/products/sales/sales/'.$sale->id,
            $editedSale
        );

        $this->assertApiResponse($editedSale);
    }

    /**
     * @test
     */
    public function test_delete_sale()
    {
        $sale = factory(Sale::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/products/sales/sales/'.$sale->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/products/sales/sales/'.$sale->id
        );

        $this->response->assertStatus(404);
    }
}

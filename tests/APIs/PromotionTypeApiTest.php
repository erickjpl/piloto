<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Products\Aditionals\PromotionType;

class PromotionTypeApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_promotion_type()
    {
        $promotionType = factory(PromotionType::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/products/aditionals/promotion_types', $promotionType
        );

        $this->assertApiResponse($promotionType);
    }

    /**
     * @test
     */
    public function test_read_promotion_type()
    {
        $promotionType = factory(PromotionType::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/products/aditionals/promotion_types/'.$promotionType->id
        );

        $this->assertApiResponse($promotionType->toArray());
    }

    /**
     * @test
     */
    public function test_update_promotion_type()
    {
        $promotionType = factory(PromotionType::class)->create();
        $editedPromotionType = factory(PromotionType::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/products/aditionals/promotion_types/'.$promotionType->id,
            $editedPromotionType
        );

        $this->assertApiResponse($editedPromotionType);
    }

    /**
     * @test
     */
    public function test_delete_promotion_type()
    {
        $promotionType = factory(PromotionType::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/products/aditionals/promotion_types/'.$promotionType->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/products/aditionals/promotion_types/'.$promotionType->id
        );

        $this->response->assertStatus(404);
    }
}

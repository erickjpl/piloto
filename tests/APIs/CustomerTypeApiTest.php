<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Customers\CustomerType;

class CustomerTypeApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_customer_type()
    {
        $customerType = factory(CustomerType::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/customers/customer_types', $customerType
        );

        $this->assertApiResponse($customerType);
    }

    /**
     * @test
     */
    public function test_read_customer_type()
    {
        $customerType = factory(CustomerType::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/customers/customer_types/'.$customerType->id
        );

        $this->assertApiResponse($customerType->toArray());
    }

    /**
     * @test
     */
    public function test_update_customer_type()
    {
        $customerType = factory(CustomerType::class)->create();
        $editedCustomerType = factory(CustomerType::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/customers/customer_types/'.$customerType->id,
            $editedCustomerType
        );

        $this->assertApiResponse($editedCustomerType);
    }

    /**
     * @test
     */
    public function test_delete_customer_type()
    {
        $customerType = factory(CustomerType::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/customers/customer_types/'.$customerType->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/customers/customer_types/'.$customerType->id
        );

        $this->response->assertStatus(404);
    }
}

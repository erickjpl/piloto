<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Config\Heritages\Contract;

class ContractApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_contract()
    {
        $contract = factory(Contract::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/config/heritages/contracts', $contract
        );

        $this->assertApiResponse($contract);
    }

    /**
     * @test
     */
    public function test_read_contract()
    {
        $contract = factory(Contract::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/config/heritages/contracts/'.$contract->id
        );

        $this->assertApiResponse($contract->toArray());
    }

    /**
     * @test
     */
    public function test_update_contract()
    {
        $contract = factory(Contract::class)->create();
        $editedContract = factory(Contract::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/config/heritages/contracts/'.$contract->id,
            $editedContract
        );

        $this->assertApiResponse($editedContract);
    }

    /**
     * @test
     */
    public function test_delete_contract()
    {
        $contract = factory(Contract::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/config/heritages/contracts/'.$contract->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/config/heritages/contracts/'.$contract->id
        );

        $this->response->assertStatus(404);
    }
}

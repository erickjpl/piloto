<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Config\Heritages\Layout;

class LayoutApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_layout()
    {
        $layout = factory(Layout::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/config/heritages/layouts', $layout
        );

        $this->assertApiResponse($layout);
    }

    /**
     * @test
     */
    public function test_read_layout()
    {
        $layout = factory(Layout::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/config/heritages/layouts/'.$layout->id
        );

        $this->assertApiResponse($layout->toArray());
    }

    /**
     * @test
     */
    public function test_update_layout()
    {
        $layout = factory(Layout::class)->create();
        $editedLayout = factory(Layout::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/config/heritages/layouts/'.$layout->id,
            $editedLayout
        );

        $this->assertApiResponse($editedLayout);
    }

    /**
     * @test
     */
    public function test_delete_layout()
    {
        $layout = factory(Layout::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/config/heritages/layouts/'.$layout->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/config/heritages/layouts/'.$layout->id
        );

        $this->response->assertStatus(404);
    }
}

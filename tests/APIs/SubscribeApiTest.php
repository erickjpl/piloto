<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Config\Suscribes\Subscribe;

class SubscribeApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_subscribe()
    {
        $subscribe = factory(Subscribe::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/config/suscribes/subscribes', $subscribe
        );

        $this->assertApiResponse($subscribe);
    }

    /**
     * @test
     */
    public function test_read_subscribe()
    {
        $subscribe = factory(Subscribe::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/config/suscribes/subscribes/'.$subscribe->id
        );

        $this->assertApiResponse($subscribe->toArray());
    }

    /**
     * @test
     */
    public function test_update_subscribe()
    {
        $subscribe = factory(Subscribe::class)->create();
        $editedSubscribe = factory(Subscribe::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/config/suscribes/subscribes/'.$subscribe->id,
            $editedSubscribe
        );

        $this->assertApiResponse($editedSubscribe);
    }

    /**
     * @test
     */
    public function test_delete_subscribe()
    {
        $subscribe = factory(Subscribe::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/config/suscribes/subscribes/'.$subscribe->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/config/suscribes/subscribes/'.$subscribe->id
        );

        $this->response->assertStatus(404);
    }
}

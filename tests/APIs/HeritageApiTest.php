<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Config\Heritages\Heritage;

class HeritageApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_heritage()
    {
        $heritage = factory(Heritage::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/config/heritages/heritages', $heritage
        );

        $this->assertApiResponse($heritage);
    }

    /**
     * @test
     */
    public function test_read_heritage()
    {
        $heritage = factory(Heritage::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/config/heritages/heritages/'.$heritage->id
        );

        $this->assertApiResponse($heritage->toArray());
    }

    /**
     * @test
     */
    public function test_update_heritage()
    {
        $heritage = factory(Heritage::class)->create();
        $editedHeritage = factory(Heritage::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/config/heritages/heritages/'.$heritage->id,
            $editedHeritage
        );

        $this->assertApiResponse($editedHeritage);
    }

    /**
     * @test
     */
    public function test_delete_heritage()
    {
        $heritage = factory(Heritage::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/config/heritages/heritages/'.$heritage->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/config/heritages/heritages/'.$heritage->id
        );

        $this->response->assertStatus(404);
    }
}

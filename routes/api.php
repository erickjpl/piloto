<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'profile'], function () {
    Route::resource('users', 'Profile\UserAPIController');
});


Route::group(['prefix' => 'config.heritages'], function () {
    Route::resource('layouts', 'Config\Heritages\LayoutAPIController');
});


Route::group(['prefix' => 'config.locale'], function () {
    Route::resource('countries', 'Config\Locale\CountryAPIController');
});


Route::group(['prefix' => 'config.locale'], function () {
    Route::resource('states', 'Config\Locale\StateAPIController');
});


Route::group(['prefix' => 'config.locale'], function () {
    Route::resource('cities', 'Config\Locale\CityAPIController');
});


Route::group(['prefix' => 'config.heritages'], function () {
    Route::resource('heritages', 'Config\Heritages\HeritageAPIController');
});


Route::group(['prefix' => 'profile'], function () {
    Route::resource('employees', 'Profile\EmployeeAPIController');
});


Route::group(['prefix' => 'config.heritages'], function () {
    Route::resource('contracts', 'Config\Heritages\ContractAPIController');
});


Route::group(['prefix' => 'products.config'], function () {
    Route::resource('categories', 'Products\Config\CategoryAPIController');
});


Route::group(['prefix' => 'products.config'], function () {
    Route::resource('brands', 'Products\Config\BrandAPIController');
});


Route::group(['prefix' => 'products.config'], function () {
    Route::resource('product_types', 'Products\Config\ProductTypeAPIController');
});


Route::group(['prefix' => 'products.config'], function () {
    Route::resource('suppliers', 'Products\Config\SupplierAPIController');
});


Route::group(['prefix' => 'products'], function () {
    Route::resource('products', 'Products\ProductAPIController');
});


Route::group(['prefix' => 'products.sales'], function () {
    Route::resource('inventories', 'Products\Sales\InventoryAPIController');
});


Route::group(['prefix' => 'products.sales'], function () {
    Route::resource('sales', 'Products\Sales\SaleAPIController');
});


Route::group(['prefix' => 'customers'], function () {
    Route::resource('customer_types', 'Customers\CustomerTypeAPIController');
});


Route::group(['prefix' => 'customers'], function () {
    Route::resource('customers', 'Customers\CustomerAPIController');
});


Route::group(['prefix' => 'products.aditionals'], function () {
    Route::resource('services', 'Products\Aditionals\ServiceAPIController');
});


Route::group(['prefix' => 'products.aditionals'], function () {
    Route::resource('promotion_types', 'Products\Aditionals\PromotionTypeAPIController');
});


Route::group(['prefix' => 'products.aditionals'], function () {
    Route::resource('promotions', 'Products\Aditionals\PromotionAPIController');
});


Route::group(['prefix' => 'billings'], function () {
    Route::resource('billings', 'Billings\BillingAPIController');
});


Route::group(['prefix' => 'billings'], function () {
    Route::resource('billing_details', 'Billings\BillingDetailAPIController');
});


Route::group(['prefix' => 'config.suscribes'], function () {
    Route::resource('plans', 'Config\Suscribes\PlanAPIController');
});


Route::group(['prefix' => 'config.suscribes'], function () {
    Route::resource('subscribes', 'Config\Suscribes\SubscribeAPIController');
});

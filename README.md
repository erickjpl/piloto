<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>


# Instalando paquetes para laravel.


### Laravel Dump Server
```
composer require --dev beyondcode/laravel-dump-server

php artisan vendor:publish --provider=BeyondCode\\DumpServer\\DumpServerServiceProvider
```
_**ejemplo de uso**_
```
Route::get('api', function(Request $request) {
    dump( $request->all() );

    return [ 'some' => 'output' ];
});

php artisan dump-server 

php artisan dump-server --format=html > dump.html
```


### Caffeinated Shinobi
```
composer require caffeinated/shinobi

php artisan vendor:publish --provider="Caffeinated\Shinobi\ShinobiServiceProvider" --tag="config"
```
_**Configuración:**_ _en el archivo **app/Http/Kernel.php** agregamos estas líneas en el array **routeMiddleware**_
```
'has.role' => \Caffeinated\Shinobi\Middleware\UserHasRole::class,
'has.permission' => \Caffeinated\Shinobi\Middleware\UserHasPermission::class,
```
_en el modelo de **User** agregamos el trait_
```
use Caffeinated\Shinobi\Concerns\HasRolesAndPermissions;

class User extends Authenticatable
{
    use HasRolesAndPermissions;

    ...
}
```
_en el archivo .env cambiar el valor del la variable **CACHE\_DRIVER=file**_
```
CACHE_DRIVER=array
```


### JWT Auth
```
composer require tymon/jwt-auth:dev-develop

php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"

php artisan jwt:secret
```
_**Configuración:**_ _en el modelo de **User** implementamos **JWTSubject** con sus metodos_
```
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
```
_en el archivo **config/auth.php** modificamos estas líneas_
```
'defaults' => [
    'guard' => 'api',
    'passwords' => 'users',
],

'guards' => [
    'web' => [
        'driver' => 'session',
        'provider' => 'users',
    ],

    'api' => [
        'driver' => 'jwt',
        'provider' => 'users',
    ],
],

'providers' => [
    'users' => [
        'driver' => 'eloquent',
        'model' => App\User::class,
    ],

    // 'users' => [
    //     'driver' => 'database',
    //     'table' => 'users',
    // ],
],
```
_en el archivo **app/Http/Middleware/Authenticate.php** modificamos estas líneas_
```
<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    // Override handle method
    public function handle($request, Closure $next, ...$guards)
    {
        if ($this->authenticate($request, $guards) === 'authentication_failed') {
            return response()->json(['error'=>'Unauthorized'],400);
        }
        return $next($request);
    }
    // Override authentication method
    protected function authenticate($request, array $guards)
    {
        if (empty($guards)) {
            $guards = [null];
        }
        foreach ($guards as $guard) {
            if ($this->auth->guard($guard)->check()) {
                return $this->auth->shouldUse($guard);
            }
        }
        return 'authentication_failed';
    }
}
```


### Laravel Cashier
```
composer require laravel/cashier

php artisan migrate

php artisan vendor:publish --tag="cashier-migrations"
```
_**Configuración:**_ _en el modelo de **User** usamos el trait **Billable**_
```
use Laravel\Cashier\Billable;

class User extends Authenticatable
{
    use Billable;
}
```
_definimos las variables_
```
CASHIER_MODEL=App\User
STRIPE_KEY=your-stripe-key
STRIPE_SECRET=your-stripe-secret
CASHIER_CURRENCY=eur
CASHIER_CURRENCY_LOCALE=nl_BE
CASHIER_LOGGER=stack
```

### InfyOm Labs
```
composer require infyomlabs/laravel-generator "7.0.x-dev"

composer require doctrine/dbal "~2.3"

php artisan infyom:publish
```
_**Configuración:** Abrir **app\Providers\RouteServiceProvider.php** y actualiza mapApiRoutes_
```
Route::prefix('api')
    ->middleware('api')
    ->as('api.')
    ->namespace($this->namespace."\\API")
    ->group(base_path('routes/api.php')); 
```


### Para poder ejecutar este demo debe ingresar a la carpeta del proyecto.

_**debe crear una base de dato:**_
```
cd ./piloto

mysql -u root -p < ./piloto.sql
```
_abra una terminal ubicado en la ruta donde se ubica el proyecto_
**ejecute los siguietes comando**
```
cp .env.example .env
npm install
npm run prod
composer install
php artisan key:generate
php artisan jwt:secret
php artisan serve
```
### **Nota:** _antes ejecutas el proyecto diferente a este servidor configura el environment **.env**s se encuentra en la raiz del proyecto, especificamente la variable **APP_URL** por defecto tiene asignada **http://localhost:8000**_

_Los datos de ingreso del usuario administrador:_
* **usuario:** *admin@mail.com*
* **clave:** *password* 

- Ya tienes listo el ambiente, [ahora hacer pruebas.](http://localhost:8000)